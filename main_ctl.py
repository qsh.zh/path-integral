import jammy.utils.hyd as hyd
import hydra
from omegaconf import OmegaConf
from pathint import PathIntegral
from env.control import Player
import numpy as np


def run(cfg):
    env = hyd.hyd_instantiate(cfg.env)
    controller = hyd.hyd_instantiate(cfg.option, env)
    integral = PathIntegral(
        env,
        controller,
        cfg.samp_zero,
        is_debug=cfg.is_debug,
        K=cfg.K,
        T=cfg.num_step,
        gamma=cfg.gamma,
    )
    player = Player(env, integral)
    player.viz_run(
        np.array([[cfg.x0, 0]]),
        cfg.aug,
        num_step=cfg.num_step,
        figname=hyd.hydpath(cfg.figname),
    )
    print(cfg.figname)


@hydra.main(config_path="conf", config_name="control.yaml")
def main(cfg):
    OmegaConf.set_struct(cfg, False)
    run(cfg)


if __name__ == "__main__":
    main()
