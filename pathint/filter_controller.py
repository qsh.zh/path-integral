import numpy as np
from einops import repeat

# TODO: the implementation could be wrong, we need to confirm the optimal control with a filtering sense
# the has no flexible, only to verify the kalman filter

class FilterController:
    def __init__(self, K, filter_, mtx_u):
        """ requires filter assume model in x=Fx+Bu+Q
        :param K: total num of sampling trajectories
        :param filter_: external filter to calculate the control command
        :param mtx_u: dx = mtx_u du
        """
        self.filter = filter_
        self.zero_input = np.zeros((K,filter_.nu))
        self.inv_mtx_u = np.linalg.inv(mtx_u)
        self.cov = None

    def step_control(self, x, cov, y):
        """control command for estimation for one step

        :param x: last state mean (K,nx)
        :param cov: last state mean (K,nx,nx)
        :param y: current state observation (K,ny)
        :return: optimal control command from filtering(K,nu), new_x(K,nx),
            new_cov(K,nx,nx)
        """        
        p_mean, p_cov = self.filter.predict(x,cov,self.zero_input)
        new_x, new_cov = self.filter.update(p_mean, p_cov, y)
        u = np.einsum('ij,kj->ki',self.inv_mtx_u,new_x - p_mean)
        return u, new_x, new_cov

    def reset(self, cov0, ob):
        """set initial distribution information and observation in batch (a bit strange)
        :param cov0: (K,nx,nx)
        :param ob: (1,T,ny)
        """        
        self.cov = cov0 # (K,nx,nx)
        self.ob = repeat(ob,'i j t->(c i) j t',c=cov0.shape[0])# (K,T,ny)
        self.cnt = 0

    def proceed(self, x):
        u, new_x, new_cov = self.step_control(x, self.cov, self.ob[:,self.cnt,:])
        self.cov = new_cov
        self.cnt += 1
        return u
