from .flt_pitgl import FltPItgl
from jammy.logging import get_logger

logger = get_logger()

class LQRPItgl(FltPItgl):
    def _samp_one_step(self, cur_state, time_t, noise):
        u = self.controller.command(cur_state, time_t)
        noise_u = u + noise  # (K, nu) noised controller command
        next_state = self.env.d_dyn_fn(cur_state, noise_u)  # (K,nx) next state
        cur_run_cost = self.env.cost_g(next_state, noise_u)  # (K,) current cost
        self.log.append(u, cur_run_cost, noise, next_state)
        # logger.debug(u)
        return noise_u - u, u, cur_run_cost, next_state
