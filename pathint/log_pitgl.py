import numpy as np
import jammy.utils.hyd as hyd
import jammy.io as jio
from .pitgl import PItgl
from jammy.logging import get_logger
from jamviz.plt import MstdDict, mstd_plot

logger = get_logger()

g_loss_data = MstdDict()
g_cnt = 0


class LogPItgl(PItgl):
    def loss_omega(self, loss):
        global g_loss_data, g_cnt
        min_ = np.min(loss)
        ext_loss = min_ + np.log(np.mean(np.exp((-loss + min_) / self.env.pi_lambda)))
        g_loss_data["loss"][g_cnt].append(loss)
        g_loss_data["ext"][g_cnt].append([ext_loss])
        g_cnt += 1
        logger.info(f"{np.mean(loss):12.6f}\t{np.std(loss):12.6f}\t{ext_loss:12.6f}")
        return super().loss_omega(loss)

    def __del__(self):
        fig, ax = mstd_plot(g_loss_data)
        fig.savefig(hyd.hydpath("data/loss_dist.png"))
        jio.dump(hyd.hydpath("data/log.pkl"), self.log)
