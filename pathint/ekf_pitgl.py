from .flt_pitgl import FltPItgl
import numpy as np
import attr
from copy import deepcopy


@attr.s
class EKFPItgl(FltPItgl):
    def __attrs_post_init__(self):
        super().__attrs_post_init__()
        self.gt_controller = deepcopy(self.controller)
        self.post_mean = []
        self.post_cov = []

    def multinomial_resample(self):
        first_step_weight, omega_weight = self._resample_weight()

        samples = self.log._log["state"][1]
        N_eff = 1.0 / np.sum(omega_weight * omega_weight)
        self.log.Neff_ratio.append(N_eff / self.K)

        mean_ = np.sum(first_step_weight.reshape(-1, 1) * samples, axis=0)
        cov_ = np.einsum("ki,kt->kit", (samples - mean_), (samples - mean_))
        if N_eff < self.K / 3:
            # fmt: off
            import ipdb; ipdb.set_trace()
            # fmt: on
            print("resampling happens", self.g_time)
            idx = self.rng.choice(
                np.arange(self.K),
                p=first_step_weight,
                size=self.K,
            )
            self.pts_weight = 1.0 * np.ones(self.K) / self.K
            samples_cov = np.sum(first_step_weight.reshape(-1, 1, 1) * cov_, axis=0)
            self.controller.reset(samples[idx], samples_cov, self.K)
            return samples[idx]
        self.pts_weight = first_step_weight
        first_cmd = self.log.origin_u[:, 0]
        state, cov = self.gt_controller.filter(np.zeros_like(first_cmd), self.env.real_obs[0])

        self.gt_controller.mean = samples
        self.controller.reset(self.gt_controller.mean, self.gt_controller.cov)

        self.post_mean.append(np.sum(first_step_weight.reshape(-1, 1) * state, axis=0))
        self.post_cov.append(np.sum(first_step_weight.reshape(-1, 1, 1) * cov, axis=0))
        return samples

    def command(self, state, time_t, is_aug=True):
        # observation is the state
        self.g_time += 1
        if self.T < self.horizon_T:
            self.T += 1

        mean, cov = self.step(state, time_t, is_aug)

        if self.T == self.horizon_T:
            self.prior_pts = self.multinomial_resample()
            # TODO: reweight!
        self.controller.reset(self.gt_controller.mean, self.gt_controller.cov)
        return mean, cov

    def _samp_one_step(self, cur_state, time_t, noise):
        # FIXME: This is wrong
        u = self.controller.command(self.env.real_obs[-time_t - 1], time_t, False)
        noise_u = u + noise  # (K, nu) noised controller command
        next_state = self.env.d_dyn_fn(cur_state, noise_u)  # (K,nx) next state
        self.controller.filter(noise_u, self.env.real_obs[-time_t-1], True)
        cur_run_cost = self.env.cost_g(next_state, noise_u)  # (K,) current cost
        self.log.append(u, cur_run_cost, noise, next_state)
        return noise_u - u, u, cur_run_cost, next_state
