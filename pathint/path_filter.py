import attr
import numpy as np


@attr.s
class PathFiltering:
    """
    Please follow the notation in the note. The dynamics equations and noise \
        need to follow the equation 1, especially pay attention to noise cov
    """

    d_dyn_fn = (
        attr.ib()
    )  # deterministic dynamics (state, action) -> next_state. This is strong assumption
    s_dyn_fn = attr.ib()  # stochastic dynamics (state, action) -> next_state
    h_fn = attr.ib()  # (state, action) -> current_cost
    obser_fn = attr.ib() # h function in the note
    controller_fn = attr.ib()  # openloop or state-feedback controller, support batch query

    nx = attr.ib()  # state dimension
    dt = attr.ib()  # delta t
    
    state_noise_dist = attr.ib(None) # FIXME: flexible and gamma coef
    init_state_dist = attr.ib(None)

    K = attr.ib(default=100)  # num of the trajectory to sample
    T = attr.ib(default=15)  # T, length of each traj
    is_debug = attr.ib(default=False)

    def sample_step(self, x):
        """choose action and arrive next state

        :param x: (K,nx)
        :return: command/noise_command(K,nu),x_next(K,nx)
        """
        command = self.controller_fn(x)
        noise = self.state_noise_dist.sample(self.K) # FIXME: flexible it
        noise_command = command + noise
        x_next = self.d_dyn_fn(x, noise_command)
        return command, noise_command, noise, x_next

    def sample_traj(self, x):
        """sample K trajectories given init states

        :param x: init state (K,nx)
        :return: states (K,T,nx), command (K,T,nu), noise(K,T,nu)
        """
        command, noise_list, x_list = [], [], []
        for i in range(self.T):
            cmd, noise_cmd, noise, x = self.sample_step(x)

            command.append(cmd)
            noise_list.append(noise)
            x_list.append(x)

        command = np.stack(command, axis=-2)
        noise = np.stack(noise_list, axis=-2)
        states = np.stack(x_list, axis=-2)
        return states, command, noise

    def eval_traj_loss(self, x_init, states, commands, noise, true_y):
        # (K,T,xxx)
        noise_cmd = noise + commands
        state_run_cost = 0.5 * self._cost_g(states, None)  # (K,T)
        action_cost = 0.5 * np.sum(noise_cmd * noise_cmd, axis=-1)  # (K,T)

        integral_dt_term = (state_run_cost + action_cost) * self.dt

        total_states = np.concatenate(
            [x_init.reshape(self.K, 1, -1), states], axix=1
        )  # (K,T+1, nx)
        total_obser = self.obser_fn(total_states)  # (K,T+1,nx)
        delta_h = total_obser[:, 1:, :] - total_obser[:, :-1, :]  # (K,T,ny)
        noise_cost = np.sum(true_y * delta_h, axis=(-1))  # (K,T)

        terminal_cost = self._terminal_state_cost(states[:, -1, :])  # (K,)

        integral_cost = np.sum(integral_dt_term + noise_cost, axis=-1)  # (K,)

        total_cost = terminal_cost + integral_cost  # (K,)

        if self.debug:
            pass

        return total_cost - np.min(total_cost)

    def estimate(self, observation, is_aug=True):
        # (K,T,ny)
        init_x = self.init_state_dist.sample(self.K)
        states, commands, noise = self.sample_traj(init_x)  # (K,T,nx)
        total_cost = self.eval_traj_loss(init_x, states, commands, noise, observation)

        weight = np.exp(-total_cost)
        eta = np.sum(weight)
        omega = 1.0 / eta * weight

        estimate = np.sum(omega.reshape(self.K, 1) * states[:, -1, :], axis=0)
        return estimate

    def _cost_g(self, state, action):
        # Attetion! This does not multi dt
        return 0.5 * (self.h_fn(state, action)) ** 2

    def _terminal_state_cost(self, state):
        # TODO: pay attention to the self._terminate_state.shape
        return np.sum(-self._terminate_state * self.h_fn(state, action), axis=-1)
