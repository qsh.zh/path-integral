import attr
import numpy as np
from utils import TrajBook


@attr.s
class PItgl:
    """
    Please follow the notation in the note. The dynamics equations and noise \
        need to follow the equation 1, especially pay attention to noise cov
    """

    env = attr.ib()
    controller = attr.ib()  # openloop or state-feedback controller, support batch query

    K = attr.ib(default=100)  # num of the trajectory to sample
    T = attr.ib(default=15)  # T, length of each traj
    gamma = attr.ib(1.0)
    samp_zero = attr.ib(True)
    log = attr.ib(None)

    nx = attr.ib(init=False)  # state dimension
    nu = attr.ib(init=False)  # command dimension
    dt = attr.ib(init=False)  # delta t

    def __attrs_post_init__(self):
        self.nx = self.env.nx
        self.nu = self.env.nu
        self.dt = self.env.dt
        if self.log is None:
            self.log = TrajBook()

    def loss_omega(self, loss):
        return self.log.omega(self.env.pi_lambda)

    def est_aug(self):
        aug = self.log.noise_aug(self.env.pi_lambda)
        return aug[0]

    def ext_costs(self, state, time_t):
        traj_costs = self.cal_costs(state, time_t)  # (K,)
        min_ = np.min(traj_costs)
        return min_ + np.log(np.mean(np.exp((-traj_costs + min_) / self.env.pi_lambda)))

    def command(self, state, time_t, is_aug=True):
        cmd = self.step(state, time_t, is_aug)
        self.T -= 1
        return cmd

    def step(self, state, time_t, is_aug=True):
        if is_aug is False:
            return self.controller.command(state, time_t)  # (1, nu)
        self.cal_costs(state, time_t)  # (K,)

        # only do improvement on current state
        cur_action = self.controller.command(state, time_t)  # (1, nu)
        aug_u = self.est_aug()

        return cur_action + aug_u

    def cal_costs(self, state, time_t):
        """sampling trajectories and calculate each cost

        :param state: init (1,nx)
        :return: [description]
        :rtype: [type]
        """
        # TODO, assert the state shape
        noise = self.env.sim_action_noise(
            (self.K, self.T, self.nu), noise_level=self.gamma
        )  # (K,T,nu)
        if self.samp_zero:
            noise[0, :, :] = 0
        self.sampling_trajs(state.repeat(self.K, axis=0), time_t, noise)
        total_cost = self.log.total_cost(self.env.R, self.dt, self.gamma)

        return total_cost

    def sampling_trajs(self, cur_state, time_t, noise):
        """calculate the traj and its cost, maybe we need to seperate the function

        :param cur_state: (K,nx)
        :param noise: (K,T,nu)
        :return: cost(K,); states(K,T,nx); action(K,T,nu); rtn_noise(K,T,nu); \
            original_noise(K,T,nu)
        """
        assert cur_state.shape == (self.K, self.nx)
        assert noise.shape == (self.K, self.T, self.nu)
        cost_total = np.zeros(self.K)  # (K,)

        self.log.start(cur_state, time_t)

        for t in range(self.T):
            _, _, _, cur_state = self._samp_one_step(cur_state, t + time_t, noise[:, t])

        self.log.end(self.env.terminal_cost(cur_state))

        return self.log

    def _samp_one_step(self, cur_state, time_t, noise):
        u = self.controller.command(cur_state, time_t)
        noise_u = u + noise  # (K, nu) noised controller command
        next_state = self.env.d_dyn_fn(cur_state, noise_u)  # (K,nx) next state
        cur_run_cost = self.env.cost_g(cur_state, noise_u)  # (K,) current cost
        self.log.append(u, cur_run_cost, noise, next_state)
        return noise_u - u, u, cur_run_cost, next_state
