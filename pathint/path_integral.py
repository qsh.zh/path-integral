import attr
import numpy as np
from utils import debug_sampling, TrajBook
from jammy.random import JamRandomState


@attr.s
class PathIntegral:
    """
    Please follow the notation in the note. The dynamics equations and noise \
        need to follow the equation 1, especially pay attention to noise cov
    """

    env = attr.ib()
    controller = attr.ib()  # openloop or state-feedback controller, support batch query
    samp_zero = attr.ib(True)

    K = attr.ib(default=100)  # num of the trajectory to sample
    T = attr.ib(default=15)  # T, length of each traj
    gamma = attr.ib(1.0)
    u_max = attr.ib(None)  # (nu,)
    u_min = attr.ib(None)  # (nu, )
    is_debug = attr.ib(default=False)
    log_costs = attr.ib(default=False)

    nx = attr.ib(init=False)  # state dimension
    nu = attr.ib(init=False)  # command dimension
    dt = attr.ib(init=False)  # delta t

    def __attrs_post_init__(self):
        self.nx = self.env.nx
        self.nu = self.env.nu
        self.dt = self.env.dt
        if self.is_debug is True:
            self.debug_cnt = 0

    def command(self, state, time_t, is_aug=True):
        if is_aug is False:
            cur_action = self.controller.command(state, time_t)  # (1, nu)
            return cur_action, np.zeros_like(cur_action), np.zeros_like(cur_action)
        cost_total = self.sampling_traj(state, time_t)  # (K,)
        cost_total -= np.min(cost_total)
        weight = np.exp(-cost_total / self.env.pi_lambda)
        eta = np.sum(weight)
        omega = 1.0 / eta * weight

        # only do improvement on current state
        cur_action = self.controller.command(state, time_t)  # (1, nu)
        aug_u = np.sum(omega.reshape(-1, 1) * self.noise[:, 0], axis=0)
        noise_u = np.mean(self.noise[:, 0], axis=0)
        if is_aug:
            cur_action += aug_u

        self.T -= 1
        return cur_action, aug_u, noise_u

    def sampling_traj(self, state, time_t):
        """sampling trajectories

        :param state: init (1,nx)
        :return: [description]
        :rtype: [type]
        """
        # TODO, assert the state shape
        noise = self.env.sim_action_noise(
            (self.K, self.T, self.nu), noise_level=self.gamma
        )  # (K,T,nu)
        if self.samp_zero:
            noise[0, :, :] = 0
        (
            sampled_cost,  # (K,)
            sampled_states,  # (K,T,nx)
            sampled_action,  # (K,T,nu)
            sampled_noise,
            origin_u,  # (K,T,nu)
        ) = self.cal_traj_costs(state.repeat(self.K, axis=0), time_t, noise)

        # action cost
        action_cost_dt = (
            np.einsum("kij,jt,kit->k", origin_u, self.env.R, origin_u) * 0.5
        )
        # noise cost
        action_noise_cost_dt = np.einsum(
            "kij,jt,kit->k", origin_u, self.env.R, sampled_noise
        )
        noise_cost_dt = (
            np.einsum(
                "kij,jt,kit->k",
                sampled_noise,
                self.env.R,
                sampled_noise,
            )
            * 0.5
            * (1 - 1.0 / self.gamma ** 2)
        )
        print(np.mean([action_cost_dt, action_noise_cost_dt, sampled_cost], axis=1))
        if self.log_costs:
            self.costs_log = {
                "run": sampled_cost,
                "act": action_cost_dt,
                "an": action_noise_cost_dt,
            }
            if self.samp_zero:
                self.costs_log["zero"] = {
                    "s": sampled_states[0],
                    "a": sampled_action[0],
                }

        whole_cost = (
            sampled_cost + action_cost_dt + action_noise_cost_dt + noise_cost_dt
        )
        total_cost = whole_cost

        if self.is_debug:
            debug_sampling(
                sampled_states,
                sampled_action,
                sampled_noise,
                origin_u,
                self.whole_traj_loss,
                0.5 * np.sum(origin_u * origin_u, axis=(2)) * self.dt,
                np.sum(origin_u * sampled_noise, axis=(2)) * self.dt,
                self.debug_cnt,
            )
            self.debug_cnt += 1

        # record the noise
        self.noise = sampled_noise
        self.sampled_cost = total_cost
        self.sampled_state = sampled_states
        self.sampled_action = sampled_action

        return total_cost

    def cal_traj_costs(self, cur_state, time_t, noise):
        """calculate the traj and its cost, maybe we need to seperate the function

        :param cur_state: (K,nx)
        :param noise: (K,T,nu)
        :return: cost(K,); states(K,T,nx); action(K,T,nu); rtn_noise(K,T,nu); \
            original_noise(K,T,nu)
        """
        assert cur_state.shape == (self.K, self.nx)
        assert noise.shape == (self.K, self.T, self.nu)
        cost_total = np.zeros(self.K)  # (K,)

        states = []
        rtn_noise = []
        origin_u = []
        whole_traj_loss = []  # debug purpose
        for t in range(self.T):
            new_noise, ctl_u, cur_run_cost, cur_state = self._samp_one_step(
                cur_state, t + time_t, noise[:, t]
            )
            # record
            rtn_noise.append(new_noise)
            origin_u.append(ctl_u)
            states.append(cur_state)
            whole_traj_loss.append(cur_run_cost)

        states = np.stack(states, axis=-2)
        rtn_noise = np.stack(rtn_noise, axis=-2)
        origin_u = np.stack(origin_u, axis=-2)
        actions = origin_u + rtn_noise
        cost_total += np.sum(np.array(whole_traj_loss) * self.dt, axis=0)

        cost_total += self.env.terminal_cost(cur_state)

        if self.is_debug:
            self.whole_traj_loss = np.array(whole_traj_loss).T * self.dt

        return cost_total, states, actions, rtn_noise, origin_u

    def _samp_one_step(self, cur_state, time_t, noise):
        u = self.controller.command(cur_state, time_t)
        noise_u = self._bound_action(u + noise)  # (K, nu) noised controller command
        next_state = self.env.d_dyn_fn(cur_state, noise_u)  # (K,nx) next state
        cur_run_cost = self.env.cost_g(cur_state, noise_u)  # (K,) current cost
        return noise_u - u, u, cur_run_cost, next_state

    def _bound_action(self, action):
        if self.u_max is not None:
            for t in range(self.T):
                # FIXME: check correctness! Necessary?
                u = action[:, self._slice_control(t)]  # (K, 1, nu)
                cu = np.max(np.min(u, self.u_max), self.u_min)
                action[:, self._slice_control(t)] = cu
        return action

    def _slice_control(self, t):
        return slice(t * self.nu, (t + 1) * self.nu)

