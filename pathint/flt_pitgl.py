from .pitgl import PItgl
from jammy.random import gen_rng
import numpy as np
import attr


@attr.s
class FltPItgl(PItgl):
    mpc = attr.ib(False)
    eff_thres = attr.ib(10)

    def __attrs_post_init__(self):
        super().__attrs_post_init__()
        self.horizon_T = self.T
        self.T = 0
        self.prior_pts = None
        self.pts_weight = np.ones((self.K,)) * 1.0 / self.K
        self.rng = gen_rng()
        self.g_time = 0
        if self.mpc:
            self.mpc_cmd = np.zeros((1, self.nu))

    def cal_costs(self, state, time_t):
        """sampling trajectories and calculate each cost

        :param state: init (1,nx)
        :return: [description]
        :rtype: [type]
        """
        # TODO, assert the state shape
        noise = self.env.sim_action_noise(
            (self.K, self.T, self.nu), self.gamma, self.rng
        )  # (K,T,nu)
        if self.samp_zero:
            noise[0, :, :] = 0
        self.sampling_trajs(state.repeat(self.K, axis=0), time_t, noise)
        total_cost = self.log.total_cost(self.env.R, self.dt, self.gamma)

        return total_cost

    def _samp_one_step(self, cur_state, time_t, noise):
        if self.mpc:
            u = np.repeat(self.mpc_cmd[time_t].reshape(1, -1), self.K, axis=0)
        else:
            u = self.controller.command(self.env.real_obs[time_t], time_t)
        noise_u = u + noise  # (K, nu) noised controller command
        next_state = self.env.d_dyn_fn(cur_state, noise_u)  # (K,nx) next state
        cur_run_cost = self.env.cost_g(next_state, noise_u)  # (K,) current cost
        self.log.append(u, cur_run_cost, noise, next_state)
        return noise_u - u, u, cur_run_cost, next_state

    def sampling_trajs(self, cur_state, time_t, noise):
        # reset the state so that the calculation of the cost is correct
        self.env.reset(cur_state)
        return super().sampling_trajs(cur_state, time_t, noise)

    def prepare_samling(self, state):
        self.env.fresh_obs(state, self.g_time)
        if hasattr(self.controller, "fresh_obs"):
            self.controller.fresh_obs(state, self.g_time)

    def cal_costs(self, state, time_t):
        """
        put observation state into the env queue, sampling init state from prior
        sampling trajectories and calculate each cost

        :param state: latest observation (1,nx)
        """
        self.prepare_samling(state)
        init_states = self.prior_pts

        noise = self.env.sim_action_noise(
            (self.K, self.T, self.nu), noise_level=self.gamma
        )  # (K,T,nu)
        if self.samp_zero:
            noise[0, :, :] = 0
            # init_states[0] = np.mean(self.prior_pts, axis=0)
        self.sampling_trajs(init_states, time_t, noise)
        total_cost = self.log.total_cost(self.env.R, self.dt, self.gamma)

        return total_cost

    def _resample_weight(self):
        # weigt final estimation
        first_step_weight = self.log.particle_update_weight() * self.pts_weight
        first_step_weight /= np.sum(first_step_weight)
        omega_weight = self.log.omega() * self.pts_weight
        omega_weight /= np.sum(omega_weight)

        return first_step_weight, omega_weight

    def multinomial_resample(self):
        first_step_weight, omega_weight = self._resample_weight()

        samples = self.log._log["state"][1]
        N_eff = 1.0 / np.sum(omega_weight * omega_weight)
        self.log.Neff_ratio.append(N_eff / self.K)
        if N_eff < self.K / self.eff_thres:
            print("resampling happens", self.g_time)
            idx = self.rng.choice(
                np.arange(self.K),
                # p=omega_weight,
                p=first_step_weight,
                size=self.K,
            )
            self.pts_weight = 1.0 * np.ones(self.K) / self.K
            # pts_weight = first_step_weight / omega_weight
            # pts_weight /= np.sum(pts_weight)
            # self.pts_weight = np.clip(pts_weight, 1e-7,None)

            return samples[idx]
        self.pts_weight = first_step_weight
        return samples

    def step(self, state, time_t, is_aug=True):
        self.cal_costs(state, time_t)  # (K,)

        return self.est_aug()

    def est_aug(self):
        mean, cov = self.log.state_aug(self.pts_weight)
        return mean[-1], cov[-1]

    def command(self, state, time_t, is_aug=True):
        # observation is the state
        self.g_time += 1
        if self.T < self.horizon_T:
            self.T += 1
        if is_aug is False:
            return self.controller.command(state, time_t, is_online=True)
        mean, cov = self.step(state, time_t, is_aug)
        if self.mpc:
            self.mpc_cmd = np.array(
                self.log.cmd_aug(self.pts_weight)[-self.horizon_T + 1 :]
            )
            self.mpc_cmd.resize(self.horizon_T, self.nu)
        if self.T == self.horizon_T:
            self.prior_pts = self.multinomial_resample()
        return mean, cov
