## TODO

- [x] Clean the urgly flags in path_integral.
- [x] Clean up API call in path_integral.
- [ ] Simple follow controller.

## Bugs

- [ ] Usage of the RatioKalman in sampling is wrong. They have self states! Should reset after each command.
