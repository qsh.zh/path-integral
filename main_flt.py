import jammy.utils.hyd as hyd
import jammy.io as jio
import hydra
import numpy as np
from jamviz.plt import plotstd
from omegaconf import OmegaConf
from jammy.utils.debug import decorate_exception_hook
import matplotlib
import matplotlib.pyplot as plt
from utils.traj_book import DetailBook
from tqdm.auto import tqdm
from jammy.logging import get_logger
import jammy.utils.env as jenv


@decorate_exception_hook
def run(cfg):
    jenv.jam_setenv("random_seed", cfg.seed)
    cfg.figname = f"{cfg.tag}/{cfg.figname}_{cfg.seed:02d}"
    logger = get_logger(hyd.hydpath(f"data/{cfg.figname}.log"), level="DEBUG")
    env = hyd.hyd_instantiate(cfg.env)
    controller = hyd.hyd_instantiate(cfg.option, env)
    hyd.hyd_instantiate(cfg.init, env, controller)()
    pi = hyd.hyd_instantiate(cfg.pi, env, controller, cfg.K, cfg.T, log=DetailBook())
    controller.zero_inputs_ = np.zeros((cfg.K, env.nu))

    samp_states, samp_obser = env.sample(cfg.L)  # (1,L+1,nx/ny)
    pi.prior_pts = env.prior_sample(cfg.K)

    # remember to reset the controller states
    pi_mean, pi_cov = [], []  # start with 1
    for i in tqdm(range(cfg.L)):
        mean, cov = pi.command(samp_obser[:, i + 1], 0)
        pi_mean.append(mean)
        pi_cov.append(cov)

    posterior_mean, posterior_std = env.sample_mean, env.sample_cov

    # viz data
    fig, axs = plt.subplots(1, 4, figsize=(4 * 7, 1 * 7))
    # axs[0].plot(samp_states[0, 1:], "r*-", label="gt", markersize=2)
    # axs.plot(obser[0, 1:] / 5, "k+", label="obs", markersize=6)
    plotstd(
        pi_mean[:],
        2 * np.sqrt(pi_cov[:]).flatten(),
        color="gray",
        ax=axs[0],
        label="PI",
        markersize=2,
    )
    plotstd(
        posterior_mean,
        2 * posterior_std,
        color="orange",
        label="Posterior",
        ax=axs[0],
        markersize=2,
    )

    m_err = np.abs(posterior_mean.flatten() - np.array(pi_mean).flatten())
    c_err = np.abs(posterior_std.flatten() - np.sqrt(pi_cov[:]).flatten())

    axs[0].legend()
    axs[1].plot(m_err)
    axs[1].set_title(
        f"{np.mean(m_err):3.4f}"
    )
    axs[2].plot(c_err)
    axs[2].set_title(
        f"{np.mean(c_err):3.4f}"
    )
    axs[3].plot(pi.log.Neff_ratio)
    axs[3].set_title("surviving rate")
    fig.savefig(hyd.hydpath(f"data/{cfg.figname}.png"))
    jio.dump(hyd.hydpath(f"data/{cfg.figname}.pkl"), {"m": m_err,"c":c_err,"ratio": pi.log.Neff_ratio})
    plt.close("all")


@hydra.main(config_path="conf", config_name="filter.yaml")
def main(cfg):
    OmegaConf.set_struct(cfg, False)
    run(cfg)


if __name__ == "__main__":
    main()
