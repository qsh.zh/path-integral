import numpy
import jammy.io as jio
import numpy as np
import matplotlib.pyplot as plt
from utils.density_mesh import viz_density

density = jio.load(f"data/density.pkl")

viz_density(np.linspace(-5,5,100), density.shape[0], density)

plt.show()