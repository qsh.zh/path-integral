import numpy as np
import attr


@attr.s
class ConstCtl:
    env = attr.ib()
    const = attr.ib(0.0)
    K = attr.ib(1)

    def __attrs_post_init__(self):
        self.nu = self.env.nu

    def command(self, y, time_t, is_online=True):
        return self.const * np.ones((self.K, self.nu))
