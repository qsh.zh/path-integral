from .batch_kalman import BatchKalman
import numpy as np
import attr

@attr.s
class BatchRTS(BatchKalman):
    def __attrs_post_init__(self):
        super().__attrs_post_init__()

    def smooth(self, cmds, observations, mean, cov):
        """do smoothing, return mean (K,T+1,nx)), List(K,T+1,nx,nx))

        :param cmds: list (K,T,nu)
        :param observations: list (K,T, ny)
        :param mean: list (K,nx)
        :param cov: list (K,nx,nx)
        """
        cached_p_mean, cached_p_cov = [],[] # T (K,nx), (K,nx,nx) prior
        cached_mean, cached_cov = [mean], [cov] # T+1 (K,nx), (K,nx,nx)
        for i in range(cmds.shape[1]):
            p_mean, p_cov = self.predict(mean, cov, cmds[:,i,:])
            cached_p_mean.append(p_mean);cached_p_cov.append(p_cov)
            mean, cov = self.update(p_mean, p_cov, observations[:,i,:])
            cached_mean.append(mean); cached_cov.append(cov)

        back_mean = [mean]
        back_cov = [cov]
        for i in range(cmds.shape[1]):
            gain = np.einsum('kij,jm,kmn->kin', cached_cov[-i-2], self.F.T, \
                np.linalg.inv(cached_p_cov[-i-1]))
            mean = cached_mean[-i-2] + np.einsum('kin,kn->ki', gain, mean - cached_p_mean[-i-1])
            
            cov = cached_cov[-i-2] + np.einsum('kin,knj,ktj->kit', gain, cov - cached_p_cov[-i-1], gain)

            back_mean.append(mean); back_cov.append(cov)
        
        back_mean.reverse(); back_cov.reverse()
        return np.stack(back_mean,axis=1), np.stack(back_cov,axis=1)

    def filter_smoother(self, cmds, observations, mean, cov):
        """do smoothing, return mean (K,T+1,nx)), List(K,T+1,nx,nx))

        :param cmds: list (K,T,nu)
        :param observations: list (K,T, ny)
        :param mean: list (K,nx)
        :param cov: list (K,nx,nx)
        """
        cached_p_mean, cached_p_cov = [],[] # T (K,nx), (K,nx,nx)
        cached_mean, cached_cov = [mean], [cov] # T+1 (K,nx), (K,nx,nx)
        for i in range(cmds.shape[1]):
            p_mean, p_cov = self.predict(mean, cov, cmds[:,i,:])
            cached_p_mean.append(p_mean);cached_p_cov.append(p_cov)
            mean, cov = self.update(p_mean, p_cov, observations[:,i,:])
            cached_mean.append(mean); cached_cov.append(cov)

        back_mean = [mean]
        back_cov = [cov]
        for i in range(cmds.shape[1]):
            gain = np.einsum('kij,jm,kmn->kin', cached_cov[-i-2], self.F.T, \
                np.linalg.inv(cached_p_cov[-i-1]))
            mean = cached_mean[-i-2] + np.einsum('kin,kn->ki', gain, mean - cached_p_mean[-i-1])
            
            cov = cached_cov[-i-2] + np.einsum('kin,knj,ktj->kit', gain, cov - cached_p_cov[-i-1], gain)

            back_mean.append(mean); back_cov.append(cov)
        
        back_mean.reverse(); back_cov.reverse()
        return np.stack(cached_mean, axis=1), np.stack(cached_cov, axis=1), \
            np.stack(back_mean,axis=1), np.stack(back_cov,axis=1)
