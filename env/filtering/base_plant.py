import numpy as np
import matplotlib.pyplot as plt
from utils import handle_batch_input
import attr
from jammy.random import gen_rng
from scipy.signal import savgol_filter
import collections


@attr.s
class BasePlant:
    # fill up the queue, reset x_0, then call as usual
    dt = attr.ib()
    sigma = attr.ib()  # notation from Yongxin's note
    T = attr.ib()
    cost_action = attr.ib(1)
    coef_u = attr.ib(1.0)
    sigma_B = attr.ib(1.0)
    gamma = attr.ib(1.0)
    rng = attr.ib(default=None)
    g_time = attr.ib(0)

    def __attrs_post_init__(self):
        # fix me
        self.sigma = 1
        if self.rng is None:
            self.rng = gen_rng()
        self.real_obs = collections.deque([], (self.T))

        self.R = np.array([[self.cost_action]]) * self.dt

    @property
    def dyn_noise(self):
        return np.eye(self.nx) * self.sigma ** 2 * self.dt

    @property
    def obs_gain(self):
        return np.eye(self.ny)

    @property
    def obs_noise(self):
        return np.eye(self.ny) / self.dt

    @handle_batch_input
    def d_dyn_fn(self, state, action):
        pass

    @handle_batch_input
    def s_dyn_fn(self, state, action):
        noise_action = self.noise_action(action, 1.0)
        return self.d_dyn_fn(state, noise_action)

    @handle_batch_input
    def gamma_dyn_fn(self, state, action):
        noise_action = self.noise_action(action, self.gamma)
        return self.d_dyn_fn(state, noise_action)

    @handle_batch_input
    def cost(self, state, action):
        cost_a_dt = self.cost_a(action) * self.dt
        cost_g_dt = self.cost_g(state, action) * self.dt
        return np.hstack((cost_a_dt, cost_g_dt)).reshape(-1, 2)

    @handle_batch_input
    def cost_g(self, state, action):
        # Y_t (1, nh)
        time_t = self.simulate_cnt
        cost = self._cost_g(time_t, state, action)
        self.simulate_cnt += 1
        return cost

    @handle_batch_input
    def _cost_g(self, time_t, state, action):
        h = self.h(time_t + 1.0, state)
        return (
            0.5 * np.sum((h - self.real_obs[time_t]) ** 2, axis=1) / (self.sigma_B ** 2)
        )

    @handle_batch_input
    def cost_a(self, action):
        return 0.5 * self.cost_action * np.sum(action * action, axis=1)

    @handle_batch_input
    def terminal_cost(self, state):
        return np.zeros((state.shape[0],))

    def noise_action(self, action, noise_level=1, rng=None):
        return action + self.sim_action_noise(action.shape, noise_level, rng)

    def sim_action_noise(self, shape, noise_level=1, rng=None):
        if rng is not None:
            return (
                rng.normal(size=shape) / (self.coef_u * np.sqrt(self.dt)) * noise_level
            )
        return (
            self.rng.normal(size=shape) / (self.coef_u * np.sqrt(self.dt)) * noise_level
        )

    def step(self, state, action):
        return self.s_dyn_fn(state, action)

    @property
    def nx(self):
        return 1

    @property
    def nu(self):
        return 1

    @property
    def ny(self):
        return 1

    @property
    def init_s(self):
        return np.array([[0.0]])

    @property
    def pi_lambda(self):
        # return self.cost_action * 1.0 / (self.coef_u) ** 2
        # In filtering problem, it is safe to use constant 1
        return 1.0

    def fresh_obs(self, new_observation, t):
        self.real_obs.append(new_observation)
        self.g_time = t

    def h(self, time_t, state):
        return state

    def reset(self, init_x):
        self.simulate_cnt = 0

    @handle_batch_input
    def obs_fn(self, time_t, state, rng=None):
        obs = self.h(time_t, state)
        if rng is None:
            return obs + self.sigma_B * self.rng.normal(size=obs.shape) / np.sqrt(
                self.dt
            )
        return obs + rng.normal(size=obs.shape) * self.sigma_B / np.sqrt(self.dt)

    def sample(self, L):
        """sample the real states and its observation

        :param L: total timestamps
        :return: mean (1,L+1,nx) cov (1,L+1,nx,nx)
        """        
        x = self.init_s
        states = [x]
        obs = [self.obs_fn(0, x)]
        for i in range(L):
            x = self.step(x, np.zeros(self.nu).reshape(1, -1))
            states.append(x)
            obs.append(self.obs_fn(i + 1, x))
        states = np.array(states).reshape(1, L + 1, self.nx)
        obs = np.array(obs).reshape(1, L + 1, self.ny)

        sim_mean, sim_std = np.mean(self.h(0, states) - obs), np.std(
            self.h(0, states) - obs
        )
        positive_noise = np.sum(self.h(0, states) > obs) / (L + 1)
        print(
            f"catch positive noise ratio: {positive_noise:3.4f}, sim_mean: {sim_mean:3.4f}, sim_std: {sim_std:3.4f}"
        )
        return states, obs

    def prior_sample(self, L):
        return self.rng.normal(size=(L, self.nx))
