from .batch_rts import BatchRTS
import attr
import numpy as np


@attr.s
class RTSCtl:
    env = attr.ib()
    ratio = attr.ib(1.0)
    T = attr.ib(None)
    cmd = attr.ib(None)

    def __attrs_post_init__(self):
        self.smoother = BatchRTS(self.env)
        self.T = self.env.T

    def smooth_cmd(self, x0, cov0, observations, is_online=True):
        """
        :param x0: (K,nx)
        :param cov0: (K,nx,nx)
        :param observations: (K, T, ny)

        return cmds: (K, T, nu)
        """
        cmds = np.zeros((x0.shape[0], observations.shape[1], self.env.nu))
        x_smooth, cov_smooth = self.smoother.smooth(cmds, observations, x0, cov0)
        # (K, T+1, nx)
        diff = x_smooth[:, 1:] - x_smooth[:, :-1]
        cmd = np.einsum("ij, ktj->kti", np.linalg.inv(self.env.B), diff)
        if is_online:
            self.cmd = cmd
        return cmd

    def command(self, y, time_t, is_online=True):
        return self.cmd[:, time_t] * self.ratio

    def reset(self, *args, **kwargs):
        # TODO
        pass
