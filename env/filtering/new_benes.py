import numpy as np
from scipy.stats import norm
import matplotlib.pyplot as plt
from utils import handle_batch_input
import attr
from jammy.random import JamRandomState
import collections
from .base_plant import BasePlant
import collections


@attr.s
class Benes(BasePlant):
    mu = attr.ib(1.0)
    h1 = attr.ib(1.0)
    h2 = attr.ib(0.0)
    cost_action = attr.ib(1)

    def __attrs_post_init__(self):
        super().__attrs_post_init__()
        self.filter_mean_window = collections.deque([self.init_s], (self.T+1))

    @handle_batch_input
    def d_dyn_fn(self, state, action):
        gain = (
            self.mu * self.sigma * np.tanh(self.mu / self.sigma * state)
            + self.sigma * action
        )
        return state + gain * self.dt

    @handle_batch_input
    def ekf_dyn_F(self, state):
        # (K, 1)
        return (
            self.mu ** 2
            * (1 - np.power(np.tanh(self.mu / self.sigma * state), 2))
            * self.dt
            + 1
        ).reshape(-1, 1, 1)

    def ekf_dfdx(self, state):
        return (
            self.mu ** 2
            * (1 - np.power(np.tanh(self.mu / self.sigma * state), 2))
        ).reshape(-1, 1, 1)

    @handle_batch_input
    def ekf_dyn_Sigma(self, state):
        return self.sigma ** 2 * self.dt

    @handle_batch_input
    def ekf_obs_H(self, state):
        return self.h1 * np.ones((state.shape[0], 1, 1))

    def ekf_obs_R(self):
        return self.sigma_B ** 2 / self.dt

    def ekf_inv_input(self):
        return 1.0 / self.sigma * np.eye(1) / self.dt

    @property
    def nx(self):
        return 1

    @property
    def nu(self):
        return 1

    @property
    def ny(self):
        return 1

    @property
    def init_s(self):
        return np.array([[1.0]])

    def cont_A(self, state):
        return self.mu * self.sigma * np.tanh(self.mu /self.sigma * state)

    @property
    def cont_B(self):
        return np.array([[self.coef_u * self.sigma]])

    @property
    def obs_jcb(self):
        return np.array([self.h1]).reshape(1,1)

    def h(self, time_t, state):
        return self.h1 * state + self.h1 * self.h2

    def groud_truth(self, T, x0, obs):
        phi = 0
        t = T * self.dt
        inner_tri = self.h1 * self.sigma * t
        phi_d = np.sinh(inner_tri)
        for i, ob in enumerate(obs):
            phi_n = np.sinh(self.h1 * self.sigma * (i + 0.5) * self.dt)
            phi += phi_n / phi_d * ob
        a = (
            self.sigma * phi * np.tanh(inner_tri)
            + (self.h2 + x0) / np.cosh(inner_tri)
            - self.h2
        )
        b = self.mu / self.h1 * np.tanh(inner_tri)
        sigma_square = self.sigma / self.h1 * np.tanh(inner_tri)
        exponenet = 2 * a * b / self.sigma * np.cosh(inner_tri) / np.sinh(inner_tri)
        w = 1 / (1 + np.exp(exponenet))

        return w, a, b, sigma_square

    def posterior(self, x0, obs):
        """filtering based on close form

        :param x0: init start point (1,)
        :param obs: (T,)
        :return: mean (T,), std(T,)
        """        
        assert obs.shape == (len(obs),)
        T = len(obs) * self.dt
        times = np.arange(len(obs) + 0) * self.dt
        times_phi_n = (times[:-1] + times[1:]) / 2
        inner_tri = self.h1 * self.sigma * times[1:]
        phi_numerator = (
            np.sinh(self.h1 * self.sigma * times[:-1])
            + np.sinh(self.h1 * self.sigma * times[1:])
        ) / 2
        # phi_numerator = np.sinh(self.h1 * self.sigma * times_phi_n)
        obs = (obs[:-1] + obs[1:]) / 2
        unormalized_phi_item = phi_numerator * obs
        unormalized_phi_int = np.cumsum(unormalized_phi_item) * self.dt
        phi = unormalized_phi_int / np.sinh(self.h1 * self.sigma * times[1:])

        a = (
            self.sigma * phi * np.tanh(inner_tri)
            + (self.h2 + x0) / np.cosh(inner_tri)
            - self.h2
        )
        b = self.mu / self.h1 * np.tanh(inner_tri)
        sigma_square = self.sigma / self.h1 * np.tanh(inner_tri)
        std = np.sqrt(sigma_square)

        exponenet = 2 * a * b / self.sigma * np.cosh(inner_tri) / np.sinh(inner_tri)
        w = 1 / (1 + np.exp(exponenet))

        # return a, b, sigma_square, w
        return (
            w * (a - b) + (1 - w) * (a + b),
            np.sqrt(sigma_square + w * (1 - w) * (2 * b) ** 2),
        )

    def filter(self, x0, obs):
        """filtering

        :param x0: init state (1,nx)
        :param obs: observation (T, ny)
        :return: mean (T,nx), cov (T,nx,nx)
        """
        x0 = x0.flatten()
        obs = obs.flatten()
        m, c = self.posterior(x0, obs)
        return m, c

    def plot(self, y, a, b, sigma_square, w):
        density = []
        for cur_a, cur_b, cur_sigma_s, cur_w in zip(a, b, np.sqrt(sigma_square), w):
            cur_den = cur_w * norm.pdf(y, cur_a - cur_b, cur_sigma_s) + (
                1 - cur_w
            ) * norm.pdf(y, cur_a + cur_b, cur_sigma_s)
            density.append(cur_den)
        return np.array(density)

    def prior_sample(self, L):
        return np.repeat(self.init_s, L, axis=0)

    @property
    def obs_noise(self):
        return np.eye(self.ny) * self.sigma_B ** 2 / self.dt

    def sample(self, L):
        mean, obs = super().sample(L) # (1, L+1, nx), (1, L+1, ny)
        p_m, p_cov =  self.filter(self.init_s[0], obs[0,:].flatten()) # mean (T,nx), cov (T,nx,nx)
        self.sample_mean = p_m # (T, nx)
        self.sample_cov = p_cov
        return mean, obs

    def fresh_obs(self, new_observation, t):
        super().fresh_obs(new_observation, t)
        self.filter_mean_window.append(self.sample_mean[t-1])