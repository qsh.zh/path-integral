import numpy as np
import matplotlib.pyplot as plt
from utils import handle_batch_input
import attr
from .base_plant import BasePlant
import collections


@attr.s
class OUPlant(BasePlant):
    kappa = attr.ib(0.5)

    def __attrs_post_init__(self):
        super().__attrs_post_init__()
        self.A = np.array([[1.0 - self.kappa * self.dt]])
        self.B = np.array([[self.coef_u * self.sigma * self.dt]]).T
        self.filter_mean_window = collections.deque([self.init_s], (self.T+1))
        self.filter_cov_window = collections.deque(np.ones(self.nx)*1e-6, (self.T+1))

    @handle_batch_input
    def d_dyn_fn(self, state, action):
        return np.einsum("ij,kj->ki", self.A, state) + np.einsum(
            "ij,kj->ki", self.B, action
        )

    @property
    def nx(self):
        return 1

    @property
    def nu(self):
        return 1

    @property
    def ny(self):
        return 1

    def ekf_inv_input(self):
        return 1.0 / self.sigma * np.eye(1) / self.dt

    def ekf_dfdx(self, state):
        return np.array([[-self.kappa]])

    @property
    def init_s(self):
        return np.array([[0.0]])

    def h(self, time_t, state):
        return state

    def posterior(self, x0, obs):
        """

        :param x0: (nx,)
        :param obs: (T+1, nx)
        :return: m (1,T, nx), cov (1, T, nx,nx)
        """        
        from .batch_rts import BatchRTS

        rts = BatchRTS(self)
        rts_mean, rts_cov = rts.smooth(
            np.zeros((1, len(obs) - 1, self.nu)),
            obs[1:].reshape(1, len(obs) - 1, self.ny),
            x0.reshape(1, self.nx),
            np.ones((1, 1, 1)) * 1e-6,
        )
        return rts_mean[:, 1:], rts_cov[:, 1:]

    def filter(self, x0, obs):
        """filtering

        :param x0: init state (1,nx)
        :param obs: observation (T, ny)
        :return: mean (T,nx), cov (T,nx,nx)
        """
        from .batch_kalman import BatchKalman

        kf = BatchKalman(self)
        # kf.reset(x0.reshape(1,self.nx), np.eye(self.nx)*1e-6)
        mean_, cov_ = x0.reshape(1,self.nx), np.eye(self.nx)[None,:]*1e-6
        kf_mean, kf_cov = [], []
        obs = obs.reshape(1,-1,self.ny)
        for i in range(obs.shape[1]):
            # mean_, cov_ = kf.filter(np.eye(1) * 0, obs[:,i])
            mean_, cov_ = kf.oracle(mean_, cov_, np.eye(1) * 0, obs[:,i])
            kf_mean.append(mean_)
            kf_cov.append(cov_)
        return np.array(kf_mean).reshape(obs.shape[1],self.nx), np.array(kf_cov).reshape(obs.shape[1], self.nx, self.nx)

    def sample(self, L):
        mean, obs = super().sample(L) # (1, L+1, nx), (1, L+1, ny)
        p_m, p_cov =  self.filter(self.init_s, obs[0,1:].flatten()) # mean (T,nx), cov (T,nx,nx)
        self.sample_mean = p_m # (T, nx)
        self.sample_cov = p_cov
        return mean, obs

    def prior_sample(self, L):
        return np.repeat(self.init_s, L, axis=0)

    @property
    def cont_A(self):
        return np.array([[-self.kappa]])

    @property
    def cont_B(self):
        return np.array([[self.coef_u * self.sigma]])

    @property
    def obs_jcb(self):
        return np.eye(1)

    def fresh_obs(self, new_observation, t):
        super().fresh_obs(new_observation, t)
        self.filter_mean_window.append(self.sample_mean[t-1])
        self.filter_cov_window.append(self.sample_cov[t-1])

    def posterior_window(self):
        L = len(self.filter_mean_window)
        mean, cov = self.filter_mean_window[-1][0], self.filter_cov_window[-1][0,0] # scalar
        rtn_m = [mean]
        rtn_c = [cov]
        for i in range(L-1):
            f_mean, f_cov = self.filter_mean_window[-1-i][0], self.filter_cov_window[-1-i][0,0]
            der_m = -self.kappa * mean + self.sigma**2 * (mean - f_mean) / f_cov
            der_c = 2 * (-self.kappa + self.sigma**2 / f_cov)*cov - self.sigma**2
            
            mean = mean - der_m * self.dt
            cov = cov - der_c * self.dt
            rtn_m.append(mean)
            rtn_c.append(cov)
        rtn_m = np.array(rtn_m).reshape(L, self.nx)
        rtn_c = np.array(rtn_c).reshape(L, self.nx, self.nx)
        return rtn_m[::-1], rtn_c[::-1]
