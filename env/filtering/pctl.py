import numpy as np
import attr
from jammy.utils.meta import run_once
import scipy.optimize as sop


@attr.s
class PCtl:
    """
    Simple P control, suitable for one dimension, u can change the x directly
    """

    env = attr.ib()
    coef_P  = attr.ib(0.00)
    coef_O = attr.ib(0.0)
    thres = attr.ib(5)

    def __attrs_post_init__(self):
        self.zero_input = None
        self.inv_x = self.env.ekf_inv_input()
        self.dt = self.env.dt
        self.T = self.env.T

        self.nx, self.ny, self.nu = self.env.nx, self.env.ny, self.env.nu

    def fresh_obs(self, state, g_time):
        self.expand_traj = np.array(self.env.filter_mean_window, dtype="float").reshape(1,-1, self.nx) # (T, nx) the real posterior positions

    def command(self, state, time_t):
        return self._command(state, time_t)

        # control target
        prev = self.expand_traj[:,time_t]
        target = self.expand_traj[:,time_t+1] # the next targeted pose (nx,)
        proposal = np.einsum('ij,kj->ki',self.inv_x, target - self.env.d_dyn_fn(prev, self.zero_inputs_[:1]))

        diff_u = np.einsum('ij,kj->ki',self.inv_x, prev - state)

        return self.coef_O * proposal + np.clip(self.coef_P * diff_u, -self.thres, self.thres)

        # next_pose = self.env.d_dyn_fn(state, self.zero_inputs_) # (K, nx)
        # diff_pose = next_pose - target[None,:] # (K,nx)
        # diff_u = np.einsum('ij,kj->ki',self.inv_x, diff_pose)

        # return np.clip(self.coef_P * diff_u, -self.thres, self.thres)

    def _command(self, state, time_t):
        L = len(self.env.real_obs)
        init_control = np.zeros((state.shape[0],L-time_t, self.nu)).flatten()
        def _lambda(control):
            control = control.reshape((state.shape[0],L-time_t, self.nu))
            _state = state
            action_loss = np.sum(control*control) * 0.5
            for i in range(time_t, L):
                _state = self.env.d_dyn_fn(_state, control[:,i - time_t])
                action_loss += np.sum(self.env._cost_g(time_t, _state, None))
            return action_loss
        res = sop.minimize(_lambda, init_control, method='SLSQP', jac=None,options={'maxiter': 5, 'disp': False,'ftol':1e-3})
        return res.x.reshape((state.shape[0],L-time_t, self.nu))[:,0]

