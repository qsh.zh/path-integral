import numpy as np
import attr


@attr.s
class LinearOpt:
    """
    From 0 to T, totally T+1 timestamp
    Finite-horizon continous time
    P0,P1,xxxx,PT=0
    TODO: adding sigma_B
    """

    env = attr.ib()
    T = attr.ib(None)

    def __attrs_post_init__(self):
        self.A = self.env.cont_A
        self.B = self.env.cont_B
        self.dt = self.env.dt
        self.H_joc = self.env.obs_jcb

        self.nx, self.ny, self.nu = self.env.nx, self.env.ny, self.env.nu

    def _solve_P(self):
        P = np.zeros((self.nx, self.nx))
        stack_p = [P]  # (T+1, nx, nx)
        for i in range(self.T):
            der = (
                -P @ self.B @ self.B.T @ P
                + self.A.T @ P
                + P @ self.A
                + self.H_joc.T @ self.H_joc
            )
            P = P + der * self.dt
            stack_p.append(P)
        P = np.array(stack_p).reshape(self.T + 1, self.nx, self.nx)
        return P[::-1]

    def _solve_y(self, observation):
        """
        observation: (1,T,ny)
        return y: start with 1
        """
        assert observation.shape == (1, self.T, self.ny)
        y = np.zeros(self.ny)
        stack_y = [y]  # (T+1, ny)
        for i in range(self.T):
            y = y + observation[0, i] * self.dt
            stack_y.append(y)
        y = np.array(stack_y).reshape(self.T + 1, self.ny)
        return y

    def _solve_gamma(self):
        y = self.y  # (T+1, ny)
        P = self.P  # (T+1, nx, nx)
        gamma = -self.H_joc.T @ y[-1].reshape(self.ny, 1)  # (nx, 1)
        stack_gamma = [gamma]  # (T+1, nx, 1)
        for i in range(self.T):
            cur_y = y[-1 - i].reshape(self.ny, 1)  # (ny, 1)
            cur_P = P[-1 - i]  # (nx,nx)
            der = (
                -cur_P @ self.B @ (self.B.T @ gamma + self.B.T @ self.H_joc.T @ cur_y)
                + self.A.T @ gamma
                + self.A.T @ self.H_joc.T @ cur_y
            )
            gamma = gamma + der * self.dt
            stack_gamma.append(gamma)
        gamma = np.array(stack_gamma).reshape(self.T + 1, self.nx)  # (T+1,nx)
        return gamma[::-1]

    def fresh_obs(self, state, g_time):
        data = np.array(self.env.real_obs, dtype="float").reshape(1, -1, self.ny) # (1, T, ny)
        self.set_obs(data[:, :])

    def set_obs(self, observation):
        self.T = observation.shape[1]
        self.y = self._solve_y(observation)
        self.P = self._solve_P()
        self.gamma = self._solve_gamma()

    def command(self, state, time_t):
        # state (K,nx)
        P, gamma, y = (
            self.P[time_t],
            self.gamma[time_t].reshape(-1, 1),
            self.y[time_t].reshape(-1, 1),
        )
        first = -np.einsum("ij,kj->ki", self.B.T @ P, state)
        second = - np.reshape(
            self.B.T @ gamma + self.B.T @ self.H_joc.T @ y, (1, self.nx)
        )
        return first + second
