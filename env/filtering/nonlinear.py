import numpy as np
import attr


@attr.s
class NonLineraOpt:
    """
    From 0 to T, totally T+1 timestamp
    Finite-horizon continous time
    """

    env = attr.ib()
    T = attr.ib(None)
    expand_traj = attr.ib(None)

    def __attrs_post_init__(self):
        self.B = self.env.cont_B
        self.dt = self.env.dt
        self.H_joc = self.env.obs_jcb
        self.inv_x = self.env.ekf_inv_input()

        self.nx, self.ny, self.nu = self.env.nx, self.env.ny, self.env.nu

    def _solve_P(self):
        P = np.zeros((self.nx, self.nx))
        stack_p = [P]  # (T+1, nx, nx)
        for i in range(self.T):
            A = self.env.ekf_dfdx(self.expand_traj[-i - 1]).reshape(self.nx, self.nx)
            for _ in range(1):
                der = (
                    -P @ self.B @ self.B.T @ P + A.T @ P + P @ A + self.H_joc.T @ self.H_joc
                )
                P = P + der * self.dt / 1
            stack_p.append(P)
        P = np.array(stack_p).reshape(self.T + 1, self.nx, self.nx)
        return P[::-1]

    def fresh_obs(self, state, g_time):
        data = np.array(self.env.real_obs, dtype="float").reshape(
            1, -1, self.ny
        )  # (1,T,ny)
        (
            self.expand_traj,
            _,
        ) = self.env.posterior_window()  # (T+1, nx) the real posterior positions
        self.proposal = np.einsum(
            "ij,kj->ki",
            self.inv_x,
            self.expand_traj[1:]
            - self.env.d_dyn_fn(
                self.expand_traj[:-1],
                np.zeros((self.expand_traj.shape[0] - 1, self.env.nu)),
            ),
        )  # (T,nu)

        self.set_obs(data[:, :])

    def set_obs(self, observation):
        self.T = observation.shape[1]
        self.P = self._solve_P()

    def command(self, state, time_t):
        coef = self.B.T @ self.P[time_t]
        return (
            -np.einsum(
                "ij,kj->ki", coef, state - self.expand_traj[time_t][None, :]
            )
            + self.proposal[time_t][None, :]
        )
