import numpy as np
import attr
from tqdm.auto import tqdm
# from jammy.random import JamRandomState
from numpy.random import RandomState


@attr.s
class Player:
    env = attr.ib()
    agent = attr.ib()
    def __attrs_post_init__(self):
        self.rng = RandomState(44)

    def run(self, state, is_aug=True, num_step=500, player_step=None):
        log_s = [state.copy()]
        log_u = []
        log_c = []
        log_aug_u, log_noise_u = [], []
        log_real_a = []
        player_step = num_step if player_step is None else player_step
        for i in range(num_step):
            cur_action, _aug_u, _noise_u = self.agent.command(state, i, is_aug)
            log_u.append(cur_action)
            log_aug_u.append(_aug_u)
            log_noise_u.append(_noise_u)
            real_act = self.env.noise_action(cur_action, rng=self.rng)
            new_state = self.env.d_dyn_fn(state, real_act)
            log_s.append(state)
            cur_cost = self.env.cost(state, cur_action)
            log_c.append(cur_cost)
            log_real_a.append((real_act - cur_action) * self.env.dt * self.env.coef_u)
            state = new_state
            print(f"{cur_action[0,0]:10.06f}\t{(real_act - cur_action)[0][0]:10.6f}\t{cur_cost[0,0]:10.6f}")

        ter_cost = self.env.terminal_cost(state)
        rtn = []
        for i, cur in enumerate(
            [log_s, log_u, log_c, log_aug_u, log_noise_u, log_real_a]
        ):
            rtn.append(np.vstack(cur))
        print("cost: ", np.sum(log_c) + ter_cost)
        return rtn

    def viz_run(
        self, state, is_aug=True, num_step=500, player_step=None, figname="toy"
    ):
        state, action, cost, aug_u, noise_u, real_u = self.run(
            state, is_aug, num_step, player_step
        )

        if figname is None:
            return
        self.env.viz_run(state, action, cost, aug_u, noise_u, real_u, figname)
