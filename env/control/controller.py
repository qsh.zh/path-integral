import numpy as np
import attr
from utils import (
    handle_batch_input,
    fd_riccati_solver,
    fd_riccati_solver_whole_F,
    fd_riccati_solver_whole,
)
from jammy.random import JamRandomState

g_rng = JamRandomState(2)


@attr.s
class SimpleController:
    env = attr.ib()
    gain = attr.ib(0.2)

    def __attrs_post_init__(self):
        self.sigma = self.env.sigma

    @handle_batch_input
    def command(self, state, time_t):
        return -self.gain * state[:, :1] / self.sigma


@attr.s
class LQRController:
    # only work for linear system
    env = attr.ib()
    T = attr.ib(30)
    cnt = attr.ib(0)

    def __attrs_post_init__(self):
        self.init()

    def init(self):
        self.F = fd_riccati_solver(
            self.env.A, self.env.B, self.env.Q, self.env.R, self.T, self.env.dt
        )
        self.Fs = fd_riccati_solver_whole_F(
            self.env.A, self.env.B, self.env.Q, self.env.R, self.T, self.env.dt
        )
        self._Ps = self.Ps(self.T + 1)
        self.q_list = self._cal_q()

    @handle_batch_input
    def command(self, state, time_t):
        value = -state @ self.Fs[time_t].T
        return value

    def Ps(self, t):
        return fd_riccati_solver_whole(
            self.env.A, self.env.B, self.env.Q, self.env.R, t, self.env.dt
        )

    def value(self, state, time_t):
        P = self._Ps[time_t]

        return 0.5 * np.einsum("ij,jk,ik->i", state, P, state) + self.q_list[time_t] / 2

    def _cal_q(self):
        q_list = [0]
        q = 0
        W = self.env.W
        for i in range(self.T):
            q += np.trace(W @ self._Ps[-i - 1])
            q_list.append(q.copy())
        q_list.reverse()
        return q_list


class HalfLQRController(LQRController):
    def command(self, state, time_t):
        value = super().command(state, time_t)
        return value / 2


@attr.s
class NoisedLQRController(LQRController):
    ratio = attr.ib(0.5)

    @handle_batch_input
    def command(self, state, time_t):
        value = super().command(state, time_t)
        # if g_rng.uniform() > self.ratio:
        #     return np.zeros_like(value)
        return value * self.ratio
