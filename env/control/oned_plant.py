import numpy as np
import matplotlib.pyplot as plt
from jammy.utils.meta import run_once
from utils import handle_batch_input
import attr
from jammy.random import JamRandomState
from scipy.signal import savgol_filter


@attr.s
class OneD:
    # Ax + Bu + dW
    dt = attr.ib()
    sigma = attr.ib()  # notation from Yongxin's note
    cost_state = attr.ib(50000)  #
    cost_action = attr.ib(1)
    coef_u = attr.ib(1.0)
    gamma = attr.ib(1.0)
    rng = attr.ib(default=None)

    def __attrs_post_init__(self):
        if self.rng is None:
            self.rng = JamRandomState(1)
        self.R = np.array([[self.cost_action]]) * self.dt
        self.A = np.array([[1.0]])
        self.B = np.array([[self.coef_u * self.sigma * self.dt]]).T
        self.Q = np.array([[self.cost_state]]) * self.dt

    @handle_batch_input
    def d_dyn_fn(self, state, action):
        return np.einsum("ij,kj->ki", self.A, state) + np.einsum(
            "ij,kj->ki", self.B, action
        )

    @handle_batch_input
    def s_dyn_fn(self, state, action):
        noise_action = self.noise_action(action, 1.0)
        return self.d_dyn_fn(state, noise_action)

    @handle_batch_input
    def gamma_dyn_fn(self, state, action):
        noise_action = self.noise_action(action, self.gamma)
        return self.d_dyn_fn(state, noise_action)

    @handle_batch_input
    def cost(self, state, action):
        cost_a_dt = self.cost_a(action) * self.dt
        cost_g_dt = self.cost_g(state, action) * self.dt
        return np.hstack((cost_a_dt, cost_g_dt)).reshape(-1, 2)

    @handle_batch_input
    def cost_g(self, state, action):
        return 0.5 * self.cost_state * np.sum(state[:, :1] * state[:, :1], axis=1)

    @handle_batch_input
    def cost_a(self, action):
        return 0.5 * self.cost_action * np.sum(action * action, axis=1)

    @handle_batch_input
    def terminal_cost(self, state):
        return self.cost_g(state, 0) * self.dt

    def noise_action(self, action, noise_level=1, rng=None):
        return action + self.sim_action_noise(action.shape, noise_level, rng)

    def sim_action_noise(self, shape, noise_level=1, rng=None):
        if rng is not None:
            return rng.normal(size=shape) / (self.coef_u * np.sqrt(self.dt)) * noise_level
        return (
            self.rng.normal(size=shape) / (self.coef_u * np.sqrt(self.dt)) * noise_level
        )

    def step(self, state, action):
        return self.s_dyn_fn(self, state, action)

    @property
    def nx(self):
        return 1

    @property
    def nu(self):
        return 1

    @property
    def pi_lambda(self):
        return self.cost_action * 1.0 / (self.coef_u) ** 2

    @property
    def W(self):
        return np.array([[self.sigma ** 2 * self.dt]])

    @property
    def init_s(self):
        return np.array([[1.0]])

    def viz_run(self, state, action, cost, aug_u, noise_u, real_u, fig_name="toy"):
        # TODO:
        fig, axes = plt.subplots(1, 3, figsize=(24, 8))
        # axes[0].plot(state[:, 1], label="v")
        axes[0].plot(state[:, 0], label="x")
        axes[0].set_title("state")
        axes[0].legend()

        axes[1].plot(action, label="o_u")
        axes[1].plot(aug_u, label="aug")
        axes[1].plot(savgol_filter(real_u.flatten(), 4 + 1, 3), label="noised_dt")
        # axes[1].plot(real_u, label="real_u")
        axes[1].plot(noise_u, label="noise_mean")
        # axes[1].plot(action - aug_u, label="origin_u")
        axes[1].set_title("action")
        axes[1].legend()

        axes[2].plot(cost[:, 0], label="action cost")
        axes[2].plot(cost[:, 1], label="running cost")
        axes[2].set_title("cost")
        axes[2].legend()

        fig.savefig(f"{fig_name}.png")
