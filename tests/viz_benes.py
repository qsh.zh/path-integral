from env.filtering.benes import Benes
import numpy as np
import matplotlib
import matplotlib.pyplot as plt


def init_env():
    dt, T, sigma, mu, h1 = 0.01, 2000, 1.0, 0.5, 0.4
    env = Benes(dt=dt, T=T, sigma=sigma, mu=mu, h1=h1)
    return env


def main():
    env = init_env()
    K = 10
    state = np.linspace(-2, 2, K).reshape(-1, 1)
    s_state = np.linspace(-2, 2, K).reshape(-1, 1)
    stack_list = [state]
    s_stack_list = [s_state]
    for i in range(env.T):
        state = env.d_dyn_fn(state, np.zeros((state.shape[0], env.nu)))
        stack_list.append(state)
        s_state = env.s_dyn_fn(s_state, np.zeros((s_state.shape[0], env.nu)))
        s_stack_list.append(s_state)

    state = np.array(stack_list).reshape(env.T + 1, state.shape[0], env.nx)
    s_state = np.array(s_stack_list).reshape(env.T + 1, s_state.shape[0], env.nx)

    fig, axs = plt.subplots(1, 2, figsize=(2 * 7, 1 * 7))
    for i in range(K):
        axs[0].plot(state[:, i, :].flatten())
        axs[1].plot(s_state[:, i, :].flatten())

    fig.savefig(f"data/benes.png")


if __name__ == "__main__":
    main()
