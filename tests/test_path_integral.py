from pathint import PathIntegral
import unittest
import numpy as np
import numpy.linalg as linalg
from utils import NoiseDist, handle_batch_input, fd_riccati_solver
import matplotlib.pyplot as plt
from matplotlib import rcParams
from jammy.random import JamRandomState

rcParams["backend"] = "Agg"

dt = 0.05
g_cov = 0.01
g_cost = 10000
g_rng = JamRandomState(1)

g_A = np.array([
    [1,dt],
    [0,1]
])

g_B = np.array([
    [0],
    [dt * np.sqrt(g_cov)]
])

g_Q = np.array([
    [g_cost, 0],
    [0,0]
])

g_R = np.array([[1]])
g_F = fd_riccati_solver(g_A,g_B,g_Q,g_R,10)
g_noise_F = g_F / 10 - 2 * np.ones_like(g_F)

@handle_batch_input
def d_dyn_fn(state, action):
    # x @ A + v @ B
    global g_cov
    A = np.array([[1, 0], [dt, 1]])
    B = np.array([[0, 1]])
    return state @ A + action @ B * dt * np.sqrt(g_cov)


def s_dyn_fn(state, action):
    # Follow the equation in the Yongxin's Note, if the noise sigma is not \
    # identity, aborbing them in the sigma term(Pay attention to the the scale 
    # on the original action)
    global g_rng, dt
    noise_action = action + g_rng.normal(size=action.shape) * dt
    return d_dyn_fn(state, noise_action)


@handle_batch_input
def cost_fn(state, action):
    return 0.0 * np.sum(action * action, axis=1) + 0.5 * g_cost * np.sum(
        state[:, :1] * state[:, :1], axis=1
    )


@handle_batch_input
def simple_control(state):
    global g_cov
    return -0.2 * state[:, :1] / np.sqrt(g_cov)

@handle_batch_input
def optimal_control(state):
    return -state@g_F.T

@handle_batch_input
def sub_optimal_control(state):
    global g_noise_F
    return -state@g_noise_F.T

def plot_traj(agent,prefix,is_aug,num_step=500):
    state = np.array([[0.5, 0]])
    state_record = [state]
    action_recrod = []
    aug_u, noise_u = [], []
    print_cost = 0
    for i in range(num_step):
        cur_action, _aug_u, _noise_u = agent.command(state,is_aug)
        aug_u.append(_aug_u * np.sqrt(g_cov))
        noise_u.append(_noise_u * np.sqrt(g_cov))
        state = s_dyn_fn(state, cur_action)
        state_record.append(state)
        action_recrod.append(cur_action)
        print_cost += cost_fn(state, cur_action) + 0.5 * np.sum(cur_action * cur_action)
    state = np.vstack(state_record)
    plt.plot(state[:, 1], label="v")
    plt.plot(aug_u, label="aug")
    plt.plot(state[:, 0], label="x")
    plt.legend()
    subfix = "Aug" if is_aug is True else "Pure"
    print("starting", subfix, "\t cost: ", print_cost)
    plt.savefig(f"{prefix}_control_{subfix}.png")
    plt.close()
class TestPathInt(unittest.TestCase):
    def initialization(self, K=10, T=10, is_debug=False):
        global g_cov,dt
        noise_dist = NoiseDist(1)
        agent = PathIntegral(
            d_dyn_fn,
            s_dyn_fn,
            cost_fn,
            # simple_control,
            # optimal_control,
            sub_optimal_control,
            2,
            1,
            None,
            None,
            dt,
            noise_dist,
            K=K,
            T=T,
            noise_sigma_inv=linalg.inv(noise_dist.cov),
            is_debug = is_debug
        )
        return agent

    def test_initialization(self):
        self.initialization()

    def test_traj_cost(self):
        agent = self.initialization()
        state = np.random.normal(size=(10, 2))
        noise = np.random.normal(size=(10, 10, 1))
        agent.cal_traj_costs(state, noise)

    def test_sample_traj(self):
        agent = self.initialization(10, 3,is_debug=True)
        state = np.array([[0.5, 0]])
        agent.sampling_traj(state)

    def test_bad_controller(self):
        global g_cov, g_cost
        g_cost = 50000
        g_cov = 0.5
        agent = self.initialization(5000, 10)
        agent.controller = simple_control
        for is_aug in [True,False]:
            plot_traj(agent,"bad",is_aug)

    def test_subopt_controller(self):
        global g_cov, g_cost
        g_cost = 10000
        g_cov = 20
        agent = self.initialization(10000, 10)
        agent.controller = sub_optimal_control
        for is_aug in [True,False]:
            plot_traj(agent,"subopt",is_aug,80)

    def test_opt_controller(self):
        global g_cov, g_cost
        # g_cost = 10000
        # g_cov = 10
        agent = self.initialization(10000, 10)
        agent.controller = optimal_control
        for is_aug in [True,False]:
            plot_traj(agent,"opt",is_aug,80)