import numpy as np
from env.filtering import OUPlant, BatchKalman, RatioKalman, BatchRTS, RTSCtl, LinearOpt
from pathint.flt_pitgl import FltPItgl
from pathint.lqr_pitgl import LQRPItgl
import matplotlib
import matplotlib.pyplot as plt
from jamviz.plt import plotstd
from utils.traj_book import DetailBook
from jammy.utils.debug import decorate_exception_hook
from jammy.logging import get_logger
from copy import deepcopy
from einops import repeat

logger = get_logger(level="DEBUG")


def init_agents():
    # plant, filter(support reset), path integral filtering
    dt, sigma, T, K = 0.1, 1, 500, 1000
    env = OUPlant(dt, sigma, T)
    kfilter = BatchKalman(env)
    ratio_ctl = RatioKalman(env, 0.0)
    ratio_ctl.reset(np.eye(1) * 0, np.eye(1) * 1, K)
    itgl = FltPItgl(env, ratio_ctl, K, T, samp_zero=True, log=DetailBook())
    itgl.T = T
    itgl.prior_pts = np.random.normal(size=(K, 1))

    return env, kfilter, itgl


def init_rts_agent(itgl, observation):
    dt, sigma, T, K = itgl.dt, itgl.env.sigma, itgl.T, itgl.K
    env = OUPlant(dt, sigma, T)
    smoother = RTSCtl(env)
    pi = FltPItgl(env, smoother, K, T, samp_zero=True, log=DetailBook())
    pi.T = T
    pi.prior_pts = np.random.normal(size=(K, 1))
    obs = repeat(observation, "t i j->(c t) i j", c=K)
    smoother.smooth_cmd(np.zeros((K, 1)), np.ones((K, 1, 1)), obs)
    return env, pi


def init_lqr_agent(itgl, observation):
    dt, sigma, T, K = itgl.dt, itgl.env.sigma, itgl.T, itgl.K
    env = OUPlant(dt, sigma, T)
    smoother = LinearOpt(env)
    pi = LQRPItgl(env, smoother, K, T, samp_zero=False, log=DetailBook())
    pi.T = T
    pi.prior_pts = np.random.normal(size=(K, 1)) * 1
    return env, pi


def sim_data(plant, T):
    # return (1,T+1,nx) real data, (1,T+1,nh), obvervation
    x = plant.init_s
    states = [x]
    obs = [plant.obs_fn(0, x)]
    for i in range(T):
        x = plant.step(x, np.zeros(plant.nu).reshape(1, -1))
        states.append(x)
        obs.append(plant.obs_fn(i + 1, x))
    return np.array(states).reshape(1, T + 1, plant.nx), np.array(obs).reshape(
        1, T + 1, plant.ny
    )


def fill_up(plant, observations):
    # obs, (1,i,nh) i>=T
    for i, cur_obs in enumerate(observations[-plant.T :]):
        # plant.fresh_obs(cur_obs)
        if i == 0:
            plant.fresh_obs(cur_obs * 0)
        else:
            plant.fresh_obs(cur_obs)


def filter_extimate(filter, observation):
    # TODO
    filter.reset()
    est = []
    for i in range(observation.shape[1]):
        est.append(filter.filter(observation[:, i]))
    return np.array(est).reshape(1, observation.shape[1], -1)


def prepare_cmp():
    # fillup plant observation up to second to the last
    # do one step estimation for path integral filtering
    # Kalman filter estimate the current observation
    # using other proposal
    pass


def track_by_cmd(itgl):
    itgl_cmd = itgl.log.cmd_aug(itgl.pts_weight)
    # track by the cmd
    states_cmd, cur_state = [], states[:, 0]
    for cur_cmd in itgl_cmd:
        cur_state = env.d_dyn_fn(cur_state, cur_cmd.reshape(1, -1))
        states_cmd.append(cur_state)
    return states_cmd


@decorate_exception_hook
def main():
    env, kfilter, itgl = init_agents()
    a_env, a_kfilter, a_itgl = init_agents()
    states, obs = sim_data(env, env.T)  # (1,T+1,nx), (1,T+1,ny)

    # run pi_filter
    fill_up(env, obs[0, :-1])  # keep the last one for task
    # FIXME need to reset the status of controller by hand
    # kfilter.reset(np.eye(1) * 0, np.eye(1) * 1, itgl.K)
    itgl.controller.reset(np.eye(1) * 0, np.eye(1) * 1, itgl.K)
    estimation = itgl.step(obs[0, -1:], 0)
    itgl_mean, itgl_cov = itgl.log.state_aug(itgl.pts_weight)

    # test another a_itgl
    fill_up(a_itgl.env, obs[0, :-1])
    a_itgl.controller.ratio = 1.0
    a_itgl.controller.reset(np.eye(1) * 0, np.eye(1) * 1, itgl.K)
    a_itgl.controller.zero_input = itgl.controller.zero_input
    a_itgl.step(obs[0, -1:], 0)
    a_itgl_mean, a_itgl_cov = a_itgl.log.state_aug(a_itgl.pts_weight)

    smoother_env, smoother_itgl = init_rts_agent(itgl, obs[:, 1:])
    fill_up(smoother_env, obs[0, :-1])
    smoother_itgl.step(obs[0, -1:], 0)
    s_itgl_mean, s_itgl_cov = smoother_itgl.log.state_aug(smoother_itgl.pts_weight)

    l_env, l_itgl = init_lqr_agent(itgl, obs[:, 1:])
    fill_up(l_env, obs[0, :-1])
    l_itgl.step(obs[0, -1:], 0)
    lqr_mean, lqr_cov = l_itgl.log.state_aug(l_itgl.pts_weight)
    # lqr_mean = l_itgl.log.state.mean(axis=0)

    pos = env.prior_sample(1)
    l_itgl.controller.set_obs(obs[:, 1:])
    poses = []
    for i in range(env.T):
        action = l_itgl.controller.command(pos, i)
        # fmt: off
        # import ipdb; ipdb.set_trace()
        # fmt: on
        pos = env.d_dyn_fn(pos, action)
        poses.append(pos)

    # RTS
    rts = BatchRTS(env)
    rts_mean, rts_cov = rts.smooth(
        np.zeros((1, itgl.T, env.nu)),
        obs[:, 1:, :],
        np.zeros((1, 1)),
        np.ones((1, 1, 1)),
    )  # (1,env.T, nx)

    # estimate by filter
    kfilter.reset(np.eye(1) * 0, np.eye(1) * 1)
    kf_mean, kf_cov = [], []
    for i in range(env.T):
        mean_, cov_ = kfilter.filter(np.eye(1) * 0, obs[:, i + 1])
        kf_mean.append(mean_)
        kf_cov.append(np.sqrt(cov_))

    fig, axs = plt.subplots(1, 2, figsize=(2 * 7, 1 * 7))
    # for i in range(min([15, itgl.K])):
    # # for i in range(itgl.K):
    #     axs.plot(itgl.log.state[i][1:], alpha=0.2, markersize=6)
    #     # print(itgl.log.origin_u)
    # axs.plot(states[0, 1:], "r*-", label="gt", markersize=6)
    # axs.plot(obs[0, 1:], "k*-", label="obs", markersize=6)
    # plotstd(kf_mean, kf_cov, color="b", ax=axs[0], label="kf")
    plotstd(
        rts_mean.flatten()[1:],
        np.sqrt(rts_cov.flatten())[1:],
        color="orange",
        ax=axs[0],
        label="rts",
    )
    axs[0].plot(np.array(poses).flatten(), label="ctl", color="red")
    # plotstd(
    # itgl_mean.flatten()[1:],
    # np.sqrt(itgl_cov.flatten()[1:]),
    # color="gray",
    # ax=axs[0],
    # label="pi",
    # )
    # plotstd(
    # a_itgl_mean.flatten()[1:],
    # np.sqrt(a_itgl_cov.flatten()[1:]),
    # color="red",
    # ax=axs[0],
    # label="kalman_pi",
    # )
    plotstd(
        s_itgl_mean.flatten()[1:],
        np.sqrt(s_itgl_cov.flatten()[1:]),
        color="green",
        ax=axs[0],
        label="smoother_pi",
    )
    plotstd(
        lqr_mean.flatten()[1:],
        np.sqrt(lqr_cov.flatten()[1:]),
        color="pink",
        ax=axs[0],
        label="lqr",
    )
    # axs.plot(np.array(states_cmd).flatten(), color="red", label="states_cmd")
    axs[0].legend()
    axs[1].plot(
        np.abs(itgl_mean.flatten()[1:] - rts_mean.flatten()[1:]),
        color="gray",
        label="pi",
    )
    # axs[1].plot(
    # a_itgl_mean.flatten()[1:] - rts_mean.flatten()[1:],
    # color="red",
    # label="kalman_pi",
    # )
    axs[1].plot(
        np.abs(s_itgl_mean.flatten()[1:] - rts_mean.flatten()[1:]),
        color="green",
        label="smoother_pi",
    )
    axs[1].plot(
        np.abs(lqr_mean.flatten()[1:] - rts_mean.flatten()[1:]),
        color="pink",
        label="lqr",
    )
    # axs[1].plot(
    # np.abs(np.array(poses).flatten() - rts_mean.flatten()[1:]),
    # color="red",
    # label="ctl",
    # )
    # axs[1].plot(
    # np.array(kf_mean).flatten() - rts_mean.flatten()[1:],
    # color="blue",
    # label="kf",
    # )
    axs[1].legend()
    fig.savefig("data/filter_com.png")
    itgl.log.observation = obs[0]
    # print(f"{itgl.log.stat['total_cost']}")
    # print(f"{a_itgl.log.stat['total_cost']}")


if __name__ == "__main__":
    main()
