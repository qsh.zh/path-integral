import unittest
from env.control.toy_plant import Plant
from env.control import Player, SimpleController, LQRController
from unittest import TestCase
from pathint import PathIntegral
import numpy as np


class TestControl(TestCase):
    @unittest.skip
    def test_viz_traj(self):
        dt, sigma, K, T = 0.05, np.sqrt(0.01), 10, 100
        num_step = 2
        is_aug = True
        env = Plant(dt, sigma, cost_state=50000)
        controller = SimpleController(sigma, gain=1)
        integral = PathIntegral(env, controller, dt, is_debug=True, K=K, T=T)
        player = Player(env, integral)
        player.viz_run(np.array([[0.5, 0]]), is_aug, num_step, "viz_traj")

    def test_bad(self):
        dt, sigma, K, T = 0.01, 5, 30000000, 10
        num_step = 10
        is_aug = True
        env = Plant(dt, sigma, cost_state=500000)
        controller = SimpleController(sigma, gain=2)
        integral = PathIntegral(env, controller, dt, is_debug=False, K=K, T=T)
        player = Player(env, integral)
        player.viz_run(
            np.array([[0.5, 0]]), is_aug, num_step=num_step, figname="toy_bad"
        )

    # def test_optimal(self):
    #     dt, sigma, K, T = 0.02, 5, 10000, 5
    #     num_step = 5
    #     is_aug = False
    #     env = Plant(dt, sigma, cost_state=500000)
    #     controller = LQRController(dt, sigma, T, cost_state=500000)
    #     integral = PathIntegral(env, controller, dt, is_debug=False, K=K, T=5)
    #     player = Player(env, integral)
    #     player.viz_run(
    #         np.array([[0.5, 0]]), is_aug, num_step=num_step, figname="toy_toy"
    #     )
