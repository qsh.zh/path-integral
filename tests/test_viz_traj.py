import unittest
from env.control.toy_plant import Plant
from env.control import Player, SimpleController, LQRController
from unittest import TestCase
from pathint import PathIntegral
import numpy as np


class TestControl(TestCase):
    @unittest.skip
    def test_viz_traj(self):
        dt, sigma, K = 0.01, 0.1, 1500000
        num_step = 1
        is_aug = True
        env = Plant(dt, sigma, cost_state=500)
        controller = LQRController(env, T=num_step)
        integral = PathIntegral(env, controller, is_debug=True, K=K, T=num_step)
        player = Player(env, integral)
        player.viz_run(np.array([[1.0, 0]]), is_aug, num_step, 1)

    def test_viz_traj(self):
        dt, sigma, K = 1, 1, 1
        num_step = 2
        is_aug = True
        env = Plant(dt, sigma, cost_state=500)
        bad_ctl = SimpleController(env, 0)
        lqr_ctl = LQRController(env, T=num_step)
        integral = PathIntegral(env, lqr_ctl, is_debug=False, K=K, T=num_step)
        init_state = np.array([[2.0, 0.2]])
        costs = integral.sampling_traj(init_state, 0)
        cost_min = np.min(costs)
        print("discrete ctl: ", costs[0])
        print(
            "path integral sampling: ",
            cost_min - np.log(np.mean(np.exp(-costs + cost_min))),
        )
        print("lqr value: ", lqr_ctl.value(init_state, 0))

        print("")
        integral.T -= 1
        action = lqr_ctl.command(init_state, 0)
        new_state = env.d_dyn_fn(init_state, action)
        costs = integral.sampling_traj(new_state, 1)
        cost_min = np.min(costs)
        print("discrete ctl: ", costs[0])
        print(
            "path integral sampling: ",
            cost_min - np.log(np.mean(np.exp(-costs + cost_min))),
        )
        print("lqr new state value: ", lqr_ctl.value(new_state, 1))
