from env.filtering.lqr import LinearOpt
from jammy.utils.debug import decorate_exception_hook
from env.filtering import OUPlant
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from jamviz.plt import plotstd


def init_agents():
    # plant, filter(support reset), path integral filtering
    dt, sigma, T, K = 0.01, 1, 500, 15000
    env = OUPlant(dt, sigma, T)
    ctl = LinearOpt(env)
    return env, ctl


def sim_data(plant, T):
    # return (1,T+1,nx) real data, (1,T+1,nh), obvervation
    x = plant.init_s
    states = [x]
    obs = [plant.obs_fn(0, x)]
    for i in range(T):
        x = plant.step(x, np.zeros(plant.nu).reshape(1, -1))
        states.append(x)
        obs.append(plant.obs_fn(i + 1, x))
    return np.array(states).reshape(1, T + 1, plant.nx), np.array(obs).reshape(
        1, T + 1, plant.ny
    )


@decorate_exception_hook
def main():
    env, ctl = init_agents()
    states, obs = sim_data(env, env.T)  # (1,T+1,nx), (1,T+1,ny)
    ctl.set_obs(obs[:, 1:])
    pos = env.prior_sample(1) + np.array([-5,0,5]).reshape(-1,1)
    poses = []
    for i in range(env.T):
        action = ctl.command(pos, i)
        pos = env.s_dyn_fn(pos, action)
        poses.append(pos)

    poses = np.array(poses).reshape(env.T, pos.shape[0],env.nx)

    post_m, post_c = env.posterior(env.init_s, obs[0])

    fig, axs = plt.subplots(1, 2, figsize=(2 * 7, 1 * 7))
    # axs.plot(states[0, 1:].flatten(), label="gt")
    # axs.plot(ctl.y[1:].flatten(), label="acc_y")
    # axs.plot(ctl.P[1:].flatten(), label="P")
    plotstd(
        post_m.flatten(),
        np.sqrt(post_c.flatten()),
        color="red",
        ax=axs[0],
        label="post",
    )
    for i in range(pos.shape[0]):
        axs[0].plot(poses[:,i].flatten(), label="ctl")
        axs[1].plot(np.abs(post_m.flatten() - poses[:,i].flatten()))
    axs[0].legend()
    fig.savefig("opt_ctl_ou.png")


if __name__ == "__main__":
    main()
