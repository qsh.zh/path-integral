from env.filtering.benes import Benes
from env.filtering.batch_ekf import BatchEKF
import matplotlib
import matplotlib.pyplot as plt
from jamviz.plt import plotstd
import numpy as np
from jammy.utils.debug import decorate_exception_hook


@decorate_exception_hook
def main():
    L = 100
    env = Benes(dt=0.01, T=10, sigma=1.0, mu=1.0, h1=0.4)
    states, obs = env.sample(L)
    d_mean, d_std = env.posterior(env.init_s[0], obs[0, :].flatten())
    ekf = BatchEKF(env)
    ekf.reset(np.eye(1) * 1, np.eye(1) * 1e-6, 1)
    kf_mean, kf_cov = [], []
    for i in range(L):
        mean_, cov_ = ekf.filter(np.eye(1) * 0, obs[:, i + 1])
        kf_mean.append(mean_)
        kf_cov.append(np.sqrt(cov_))

    ekf.reset(np.eye(1) * 1, np.eye(1) * 1e-6, 1)
    u_state = []
    cur_state = np.eye(1).reshape(1, 1)
    for i in range(L):
        u = ekf.command(obs[:, i + 1], 0)
        cur_state = env.d_dyn_fn(cur_state, u)
        u_state.append(cur_state)

    fig, axs = plt.subplots(1, 1)
    plotstd(kf_mean, kf_cov, color="b", ax=axs, label="ekf")
    plotstd(d_mean, d_std, color="g", ax=axs, label="post")
    axs.plot(states[0, 1:], "r*-", label="gt", markersize=6)
    axs.plot(np.array(u_state).flatten(), "orange", label="ekf_u", markersize=6)
    # axs.plot(obs[0, 1:], "k*-", label="obs", markersize=6)
    axs.legend()
    fig.savefig("data/ekf.png")


if __name__ == "__main__":
    main()
