import unittest
from env.control.toy_plant import Plant
from env.control import Player, SimpleController, LQRController
from unittest import TestCase
from pathint import PathIntegral
import numpy as np
import matplotlib
import matplotlib.pyplot as plt

plt.style.use("paper")


class TestLQR(TestCase):
    def test_lqrv(self):
        dt, sigma, T = 0.01, 5, 40
        cost_state, cost_action = 1, 1
        num_step = T
        is_aug = True
        controller = LQRController(
            dt, sigma, T, cost_state=cost_state, cost_action=cost_action
        )

        x_lim, v_lim = 2.0, 2.0
        N = 100
        x = np.linspace(-x_lim, x_lim, N)
        v = np.linspace(-v_lim, v_lim, N)
        xx, vv = np.meshgrid(x, v)
        xv = np.vstack([xx.flatten(), vv.flatten()]).T

        Ps = controller.Ps(T)
        fig, axs = plt.subplots(T // 2, 2, figsize=(14, 7 * T // 2))
        q = 0
        W = np.array([[0.0, 0.0], [0.0, sigma ** 2 * dt]])
        v_max = np.max(Ps) / 2
        for i, P in enumerate(Ps):
            cost = np.einsum("ij,jk,ik->i", xv, Ps[-i - 1], xv) + q
            axs[i // 2, i % 2].pcolormesh(
                xx,
                vv,
                cost.reshape(xx.shape),
                shading="auto",
                vmax=2 * v_max,
                vmin=-2 * v_max,
            )
            q += np.trace(W @ Ps[-i - 1])
            axs[i // 2, i % 2].set_title(10 - i)
            # print(Ps[-i - 1])
        plt.savefig("lqr_v.png")
        plt.close(fig)
