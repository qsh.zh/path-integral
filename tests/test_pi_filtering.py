import numpy as np
from unittest import TestCase
from utils import OU, KalmanFilter, BatchKalman, NoiseDist
from pathint import PathFiltering, FilterController
from jammy.random import JamRandomState
import matplotlib.pyplot as plt
from matplotlib import rcParams

rcParams["backend"] = "Agg"

g_rng = JamRandomState(8)

def simple_ou():
    ou = OU(K=1, rng =g_rng)
    ou.setup()
    return ou

class TestOU(TestCase):
    def test_viz(self):
        # viz the ou process and raw observation
        ou = simple_ou()
        x = ou.sample_traj()[0]
        plt.plot(x.flatten(), label="x")
        plt.plot(ou.obser_traj(x), label="y")
        plt.legend()
        plt.savefig("test_ou.png")


    def test_kf(self):
        ou = simple_ou()
        filter = KalmanFilter(
            ou.A,
            np.zeros((1,1)),
            ou.Q,
            np.eye(1),
            ou.R,
            ou.m0,
            ou.p0
        )
        x = ou.sample_traj() # (1,T,1)
        y = ou.obser_traj(x) # (1,T,1)
        zero_u = np.zeros(1)
        predict = []
        for i_th, cur_y in enumerate(y.flatten()):
            if i_th %10 == 0:
                cur_y = None
            mean, _ = filter.estimate(zero_u, cur_y)
            predict.append(mean)

        plt.plot(x.flatten(), label="x")
        plt.plot(np.arange(x.size)[::10], y.flatten()[::10], label="y")
        plt.plot(predict, label="kf")
        plt.legend()
        plt.savefig("test_ou_kf.png")


    def test_sample_traj(self):
        K = 10
        ou = simple_ou()
        ou.T = 20
        x = ou.sample_traj()[:,1:,:] #(1,T,nx)
        y = ou.obser_traj(x) #(1,T,ny)
        filter_ = BatchKalman(
            ou.A,
            np.eye(1),
            ou.Q,
            np.eye(1),
            ou.R,
            None,None
        )
        controller = FilterController(K, filter_, ou.sde_sigma)
        path_filter = PathFiltering(
            ou.d_dyn_fn,
            ou.s_dyn_fn,
            ou.h,
            ou.h,
            controller.proceed,
            1,
            ou.dt,
            NoiseDist(1),
            None,
            K,
            ou.T
        )
        x0 = np.repeat(ou.m0[None,:],K,axis=0)
        cov0 = np.repeat(ou.p0[None,:],K,axis=0)
        controller.reset(cov0, y)
        states, cmds, noise = path_filter.sample_traj(x0)
        plt_states = states.reshape(K, ou.T) # some to plot
        plt.plot(x.flatten(),label="x")
        plt.plot(y.flatten(), label="y")
        for traj in plt_states:
            plt.plot(traj)
        plt.legend()
        plt.savefig("filter_trajs.png")





    def test_traj_costs(self):
        # show the costs of sampled traj
        pass

    def test_pi_estimation(self):
        # show the estimation with time
        pass
