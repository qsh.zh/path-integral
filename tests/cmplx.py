from env.control.toy_plant import Plant
from env.control.oned_plant import OneD
from env.control import Player, SimpleController, LQRController, NoisedLQRController
from pathint import PathIntegral
import numpy as np
import attr
from jammy.random import JamRandomState
from jamviz.plt import MstdDict, mstd_plot

g_rng = JamRandomState(1)


def extract_points(values, num_sample):
    assert not num_sample > len(values)
    points = g_rng.choice(values, size=num_sample)
    _min = np.min(points)
    hat_v = _min - np.log(np.mean(np.exp(-points + _min)))
    return points, hat_v


@attr.s
class TestProposal:
    env = attr.ib()
    lqr_ctl = attr.ib()
    steps = attr.ib(5)
    K = attr.ib(int(1e6))

    def __attrs_post_init__(self):
        self.init_state = self.env.init_s

    def data_cmplx(self, proposal, name):
        itg = PathIntegral(
            self.env, proposal, is_debug=False, K=self.K, T=self.steps, samp_zero=False
        )
        costs = itg.sampling_traj(self.init_state, 0)
        exp_range = int(np.log10(self.K))
        plt_data = MstdDict()
        for i in range(1, exp_range + 1):
            points, hat = extract_points(costs, 10 ** i)
            plt_data["mean"][i].append(points)
            plt_data["est"][i].append(hat)
            # print(f"{i} est {hat}")
        fig, ax = mstd_plot(plt_data)
        ax.axhline(y=self.lqr_ctl.value(self.init_state, 0), color="r")
        fig.savefig(f"data/{name}.png")

    def action(self, proposal):
        itg = PathIntegral(
            self.env, proposal, is_debug=False, K=self.K, T=self.steps, samp_zero=False
        )
        action, _, _ = itg.command(self.init_state, 0)
        print("proposal: ", proposal.command(self.init_state, 0))
        print("PI: ", action)
        print("LQR: ",self.lqr_ctl.command(self.init_state, 0))

    def dt_cmplx(self):
        # only works for Plant experiment
        plt_data = MstdDict()
        for i in [1, 10, 100, 500]:
            dt = 1.0 / i
            T = i
            env = Plant(dt, self.env.sigma, cost_state=500)
            lqr_ctl = LQRController(env, T)
            n_lqr = NoisedLQRController(env, T, ratio=0.3)
            itg = PathIntegral(
                env, n_lqr, is_debug=False, K=self.K, T=T, samp_zero=False
            )
            costs = itg.sampling_traj(self.init_state, 0)
            points, hat = extract_points(costs, len(costs))
            plt_data["mean"][i].append(points)
            plt_data["est"][i].append(hat)
        fig, ax = mstd_plot(plt_data)
        ax.set_xscale("log")
        ax.axhline(y=lqr_ctl.value(self.init_state, 0), color="r")
        fig.savefig(f"data/dt.png")

def test_oneD():
    sigma, dt, steps = 1, 0.001, 20
    env = OneD(dt, sigma, cost_state=10)
    lqr_ctl = LQRController(env, steps)
    worker = TestProposal(env, lqr_ctl, steps,K=100000)
    worker.data_cmplx(lqr_ctl, "lqr_oned_cmplx")
    sim_ctl = SimpleController(env, 0.0)
    worker.data_cmplx(sim_ctl, "sim_oned_cmplx")
    n_lqr = NoisedLQRController(env, steps, ratio=0.5)
    worker.data_cmplx(n_lqr, "nlqr_oned_cmplx")
    worker.action(sim_ctl)
    # worker.dt_cmplx()

def test_toy():
    sigma, dt, steps = 1, 0.01, 5
    env = Plant(dt, sigma, cost_state=1000)
    lqr_ctl = LQRController(env, steps)
    worker = TestProposal(env, lqr_ctl, steps,K=2000000)
    # sim_ctl = SimpleController(env, 0.0)
    # n_lqr = NoisedLQRController(env, steps, ratio=0.5)
    # worker.data_cmplx(lqr_ctl, "lqr_toy_cmplx")
    # worker.data_cmplx(sim_ctl, "sim_toy_cmplx")
    # worker.data_cmplx(n_lqr, "nlqr_toy_cmplx")
    worker.action(lqr_ctl)

if __name__ == "__main__":
    # test_oneD()
    test_toy()
