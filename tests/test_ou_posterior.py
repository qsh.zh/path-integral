from env.filtering.ou_plant import OUPlant
from env.filtering.batch_rts import BatchRTS


from jammy.utils.debug import decorate_exception_hook

import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from jamviz.plt import plotstd

def posterior_ou(env, x0, obs):
    rts = BatchRTS(env)
    f_m, f_c, rts_mean, rts_cov = rts.filter_smoother(
        np.zeros((1, len(obs) - 1, env.nu)),
        obs[1:].reshape(1, len(obs) - 1, env.ny),
        x0.reshape(1, env.nx),
        np.ones((1, 1, 1)) * 1e-6,
    )
    return f_m[:,1:], f_c[:,1:], rts_mean[:, 1:], rts_cov[:, 1:]


def init_agents():
    # plant, filter(support reset), path integral filtering
    dt, sigma, T, K = 0.01, 1, 500, 15000
    env = OUPlant(dt, sigma, T)
    return env

@decorate_exception_hook
def main():
    env = init_agents()
    state, obs = env.sample(env.T) # T instead of T+1
    for i in range(obs.shape[1]-1):
        env.fresh_obs(obs[:,i+1],i+1)

    pf_m, pf_c, real_m, real_cov = posterior_ou(env, env.init_s[0], obs[0,:].flatten())

    ekf_m, ekf_c = env.posterior_window()

    fig, axs = plt.subplots(1, 2, figsize=(2 * 7, 1 * 7))
    plotstd(
        real_m.flatten(),
        np.sqrt(real_cov.flatten()),
        color="red",
        ax=axs[0],
        label="discrete_posterior",
    )
    plotstd(
        pf_m.flatten(),
        np.sqrt(pf_c.flatten()),
        color="orange",
        ax=axs[1],
        label="discrete_kalman",
    )
    plotstd(
        ekf_m.flatten()[1:],
        np.sqrt(ekf_c.flatten())[1:],
        color="green",
        ax=axs[0],
        label="continuous_posterior",
    )

    plotstd(
        env.sample_mean.flatten(),
        np.sqrt(env.sample_cov.flatten()),
        color="blue",
        ax=axs[1],
        label="kf",
    )
    axs[0].legend()
    axs[1].legend()
    fig.savefig("data/ou_posterior_window.png")

if __name__ == "__main__":
    main()
