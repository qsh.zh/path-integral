from utils import KalmanFilter, BatchKalman
import numpy as np
from unittest import TestCase
from jammy.random import JamRandomState

dim = 3
T = 10
g_rng = JamRandomState(0)
F = g_rng.normal(size=(dim, dim))
B = g_rng.normal(size=(dim, dim))
H = g_rng.normal(size=(dim, dim))
R = np.diag(np.exp(g_rng.normal(size=dim)))
Q = np.diag(np.exp(g_rng.normal(size=dim)))

x0 = np.zeros((dim, 1))
cov0 = np.ones((dim,dim))


def observation():
    x = x0 + np.sqrt(cov0) @ g_rng.normal(size=x0.shape)
    act, ob = [],[]
    for _ in range(T):
        action = g_rng.normal(size=x.shape)
        ob.append(H @ x + np.sqrt(R) @ g_rng.normal(size=x.shape))
        x = F @ x + B @ action + np.sqrt(Q) @ g_rng.normal(size=x.shape)
        act.append(action)
    return np.concatenate(ob).reshape(T, dim), np.concatenate(act).reshape(T,dim)

class TestKalman(TestCase):
    def test_kalman(self):
        kalmanfilter = KalmanFilter(F,B,Q,H,R,x0.reshape(dim,1),cov0.reshape(dim,dim))
        batchfilter = BatchKalman(F,B,Q,H,R,x0.reshape(1,dim),cov0[None,:])
        u_1, y_1 = observation() # (T,dim)
        for i in range(T-1):
            k_mean,k_cov = kalmanfilter.estimate(u_1[i:i+1,:].T,y_1[i:i+1,:].T)
            b_mean,b_cov = batchfilter.estimate(u_1[i:i+1].reshape(1,dim), y_1[i:i+1,:].reshape(1,dim))
            np.allclose(k_mean.reshape(1,dim),b_mean)
            np.allclose(k_cov.reshape(1,dim,dim),b_cov)

    def test_2D(self):
        kalmanfilter = KalmanFilter(F,B,Q,H,R,x0.reshape(dim,1),cov0.reshape(dim,dim))
        batchfilter = BatchKalman(F,B,Q,H,R,\
            np.repeat(x0.reshape(1,dim),2,axis=0),
            np.repeat(cov0[None,:],2,axis=0)
            )
        u_1, y_1 = observation() # (T,dim)
        u_2, y_2 = observation() # (T,dim)
        for i in range(T-1):
            k1_mean,k1_cov = kalmanfilter.estimate(u_1[i:i+1,:].T,y_1[i:i+1,:].T)
            k2_mean,k2_cov = kalmanfilter.estimate(u_2[i:i+1,:].T,y_2[i:i+1,:].T)
            b_mean,b_cov = batchfilter.estimate(
                np.concatenate([u_1[i:i+1].reshape(1,dim), u_2[i:i+1].reshape(1,dim)]),
                np.concatenate([y_1[i:i+1,:].reshape(1,dim), y_2[i:i+1,:].reshape(1,dim)]),
            )
            np.allclose(k1_mean.reshape(dim),b_mean[0])
            np.allclose(k2_mean.reshape(dim),b_mean[1])
            np.allclose(k1_cov.reshape(dim,dim),b_cov[0])
            np.allclose(k2_cov.reshape(dim,dim),b_cov[1])
