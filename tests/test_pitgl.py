import unittest
from pathint import PItgl, LogPItgl
from env.control.toy_plant import Plant
from env.control import SimpleController, LQRController
import numpy as np

def init_base(steps):
    sigma, dt = 1, 0.01
    env = Plant(dt, sigma, cost_state=10)
    lqr_ctl = LQRController(env, steps)
    return env, lqr_ctl

def test_itgl():
    steps,K = 10,int(1000000)
    env, lqr_ctl = init_base(steps)
    sim_ctl = SimpleController(env, 0)
    itgl = LogPItgl(env, lqr_ctl,T=steps,K=K,samp_zero=False)
    # lqr_value = lqr_ctl.value(env.init_s, 0)
    # itgl_value = itgl.ext_costs(env.init_s, 0)
    lqr_cmd = lqr_ctl.command(env.init_s, 0)
    itgl_cmd = itgl.command(env.init_s, 0)

    # print(lqr_value, itgl_value)
    print(lqr_cmd, itgl_cmd)
    # print(itgl.log.)
    # itgl.log.running_loss(env.R,env.dt)

if __name__ == "__main__":
    test_itgl()