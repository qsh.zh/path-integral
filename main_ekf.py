import jammy.utils.hyd as hyd
import hydra
import numpy as np
from jamviz.plt import plotstd
import jammy.io as jio
from omegaconf import OmegaConf
from pathint.ekf_pitgl import EKFPItgl
from jammy.utils.debug import decorate_exception_hook
import matplotlib
import matplotlib.pyplot as plt
from utils.traj_book import DetailBook
from utils.density_mesh import pmesh
from tqdm.auto import tqdm


@decorate_exception_hook
def run(cfg):
    env = hyd.hyd_instantiate(cfg.env)
    controller = hyd.hyd_instantiate(cfg.option, env)
    hyd.hyd_instantiate(cfg.init, env, controller)()
    pi = EKFPItgl(
        env, controller, cfg.K, cfg.T, samp_zero=cfg.samp_zero, log=DetailBook()
    )

    states, obser = env.sample(cfg.L)  # (1,L+1,nx/ny)
    pi.prior_pts = env.prior_sample(cfg.K)
    controller.reset(pi.prior_pts, np.eye(1) * 1e-13, 1)
    pi.gt_controller.reset(pi.prior_pts, np.eye(1) * 1e-13, 1)
    pi.log.real_data = (states, obser)

    # for-loop, remember to reset the controller states
    pi_mean, pi_cov, zero_cmd_mean = [], [], []
    for i in tqdm(range(cfg.L)):
        cur_obser = obser[:, i + 1]
        mean, cov = pi.command(cur_obser, 0)
        pi_mean.append(mean)
        pi_cov.append(cov)
        zero_cmd_mean.append(pi.log.state[0,-1])
        # fig, axs = plt.subplots(1, 3, figsize=(21, 7))
        # for t in range(10):
        # axs[0].plot(pi.log.state[t])
        # axs[1].plot(pi.log.origin_u[t])
        # axs[2].plot(pi.log.noise[t])
        # # axs[0].plot((obser[0, : i + 1] - states[:, i + 1]) / cfg.env.dt, color="black")
        # fig.savefig(hyd.hydpath(f"data/{i:04d}.png"))
        # # controller.reset(pi.prior_pts, cov, cfg.K)

    controller.reset(env.init_s[0], np.eye(1) * 1e-6, 1)
    kf_mean, kf_cov = [], []
    for i in range(cfg.L):
        mean_, cov_ = controller.filter(np.eye(1) * 0, obser[:, i + 1])
        kf_mean.append(mean_)
        kf_cov.append(np.sqrt(cov_))

    # TODO: viz data
    fig, axs = plt.subplots(1, 3)
    d_mean, d_std = env.posterior(env.init_s[0], obser[0, :].flatten())
    # pos_range = np.linspace(-5, 5, 100)
    # density = env.plot(pos_range, a, b, sigma_square, w)
    # fig, axs = pmesh(pos_range, cfg.L, density)
    axs[0].plot(states[0, 1:], "r*-", label="gt", markersize=2)
    # axs.plot(obser[0, 1:] / 5, "k+", label="obs", markersize=6)
    axs[0].plot(zero_cmd_mean, 'k*-', label="zero_ekf", markersize=2)
    # no need to plot the original estimation
    plotstd(
        pi_mean[:],
        2 * np.sqrt(pi_cov[:]).flatten(),
        color="gray",
        ax=axs[0],
        label="pi",
        markersize=2,
    )
    plotstd(d_mean, 2 * d_std, color="orange", label="post", ax=axs[0], markersize=2)
    plotstd(kf_mean, kf_cov, color="blue", label="ekf", ax=axs[0], markersize=2)
    plotstd(
        np.array(pi.post_mean).flatten(),
        np.array(pi.post_cov).flatten(),
        color="black",
        label="post_ekf",
        ax=axs[0],
        markersize=2,
    )
    axs[0].legend()
    axs[1].plot(np.abs(d_mean.flatten() - np.array(pi_mean).flatten()))
    axs[2].plot(np.abs(d_std - np.sqrt(pi_cov[:]).flatten()))
    # jio.dump(hyd.hydpath(f"data/density.pkl"), density)
    fig.savefig(hyd.hydpath(f"data/{cfg.figname}.png"))


@hydra.main(config_path="conf", config_name="filter.yaml")
def main(cfg):
    OmegaConf.set_struct(cfg, False)
    run(cfg)


if __name__ == "__main__":
    main()
