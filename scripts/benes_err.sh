HYDRA_FULL_ERROR=1 python main_flt.py -m option=const env=benes L=600 T=1 figname=sir K=500 "seed=range(1,50)" tag=benes_err pi.eff_thres=5

HYDRA_FULL_ERROR=1 python main_flt.py -m option=const env=benes L=600 T=20 figname=const K=800 "seed=range(1,50)" tag=benes_err pi.eff_thres=5

HYDRA_FULL_ERROR=1 python main_flt.py -m option=n pi=lqr env=benes L=600 T=20 figname=lqr K=2000 "seed=range(1,50)" tag=benes_err pi.eff_thres=5