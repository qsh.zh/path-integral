HYDRA_FULL_ERROR=1 python main_flt.py -m option=const env=ou L=600 T=1 figname=ou_sir K=500 "seed=range(1,50)" tag=ou_no_resample pi.eff_thres=500

HYDRA_FULL_ERROR=1 python main_flt.py -m option=const env=ou L=600 T=20 figname=ou_const K=800 "seed=range(1,50)" tag=ou_no_resample pi.eff_thres=800

HYDRA_FULL_ERROR=1 python main_flt.py -m option=ln pi=lqr env=ou L=600 T=20 figname=ou_lqr K=1000 "seed=range(1,50)" pi.eff_thres=1000 tag=ou_no_resample

