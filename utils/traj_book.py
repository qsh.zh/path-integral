from collections import defaultdict
import jammy.utils.hyd as hyd
import jammy.io as jio
import numpy as np
from jamviz.plt import MstdDict, mstd_plot

# from jammy.logging import get_logger
# logger = get_logger()

g_plt_data = MstdDict()
g_plt_cnt = 0


class TrajBook:
    # Used it to track every details about sampling in path integral
    def __init__(self):
        self._log = defaultdict(list)
        self.time_t = None
        self.terminal = None
        self.stat = {}
        self.Neff_ratio = []

    def start(self, x0, time_t):
        self._log = defaultdict(list)
        self.time_t = time_t
        self.stat = {}

    def append(self, origin_u, cost_g, noise, new_state):
        self._log["cost_g"].append(cost_g)
        self._log["noise"].append(noise)
        self._log["origin_u"].append(origin_u)

    def end(self, terminal_cost):
        self.terminal = terminal_cost

    def extract(self):
        return np.stack(self._log["cost_g"]), *list(
            map(lambda key: np.stack(self._log[key], axis=-2), ["noise", "origin_u"])
        )

    @property
    def noise(self):
        return np.stack(self._log["noise"], axis=-2)

    @property
    def origin_u(self):
        return np.stack(self._log["origin_u"], axis=-2)

    def total_cost(self, R, dt, gamma=1.0):
        rtn = self.running_loss(R, dt, gamma)
        return rtn[-1]

    def omega(self, pi_lambda=1):
        global g_plt_cnt
        assert "t_c" in self.stat
        if "omega" in self.stat:
            return self.stat["omega"]
        loss = self.stat["t_c"].copy()
        min_ = np.min(loss)
        ext_loss = min_ + np.log(np.mean(np.exp((-loss + min_) / pi_lambda)))
        g_plt_data["loss"][g_plt_cnt].append(loss)
        g_plt_data["ext"][g_plt_cnt].append([ext_loss])
        g_plt_cnt += 1
        # logger.info(f"{np.mean(loss):12.6f}\t{np.std(loss):12.6f}\t{ext_loss:12.6f}")

        loss -= min_
        weight = np.exp(-loss / pi_lambda)
        eta = np.sum(weight)
        omega = 1.0 / eta * weight
        self.stat["omega"] = omega
        return omega

    def noise_aug(self, pi_lambda=1):
        if "omega" not in self.stat:
            self.omega(pi_lambda)
        omega = self.stat["omega"]
        aug = np.sum(omega.reshape(-1, 1, 1) * self.noise, axis=0)
        self.stat["noise_aug"] = aug
        return aug

    def running_loss(self, R, dt, gamma=1.0):
        origin_u = np.stack(self._log["origin_u"], axis=-2)
        real_noise = np.stack(self._log["noise"], axis=-2)
        g_cost = np.stack(self._log["cost_g"])
        # action cost
        action_cost_dt = np.einsum("kij,jt,kit->k", origin_u, R, origin_u) * 0.5
        # noise cost
        action_noise_cost_dt = np.einsum("kij,jt,kit->k", origin_u, R, real_noise)
        g_cost_dt = np.sum(g_cost, axis=0) * dt
        noise_cost_dt = (
            np.einsum(
                "kij,jt,kit->k",
                real_noise,
                R,
                real_noise,
            )
            * 0.5
            * (1 - 1.0 / gamma ** 2)
        )
        total_cost = (
            g_cost_dt
            + action_cost_dt
            + action_noise_cost_dt
            + noise_cost_dt
            + self.terminal
        )
        # import ipdb; ipdb.set_trace()
        self.stat.update(
            {
                "g_c": g_cost_dt,
                "a_c": action_cost_dt,
                "an_c": action_noise_cost_dt,
                "n_c": noise_cost_dt,
                "t_c": total_cost,
            }
        )

        return (
            g_cost_dt,
            action_cost_dt,
            action_noise_cost_dt,
            noise_cost_dt,
            self.terminal,
            total_cost,
        )

    def __del__(self):
        fig, ax = mstd_plot(g_plt_data)
        fig.savefig(hyd.hydpath("data/loss_dist.png"))
        jio.dump(hyd.hydpath("data/log.pkl"), self.stat)


class DetailBook(TrajBook):
    def start(self, x0, time_t):
        super().start(x0, time_t)
        self._log["state"].append(x0)

    def append(self, origin_u, cost_g, noise, new_state):
        self._log["state"].append(new_state)
        super().append(origin_u, cost_g, noise, new_state)

    @property
    def state(self):
        return np.stack(self._log["state"], axis=-2)

    def u_aug(self, pi_lambda=1):
        if "omega" not in self.stat:
            self.omega(pi_lambda)
        omega = self.stat["omega"]
        aug = np.sum(omega.reshape(-1, 1, 1) * (self.noise + self.origin_u), axis=0)
        self.stat["u_aug"] = aug
        return aug

    def cmd_aug(self, particle_weight):
        if "omega" not in self.stat:
            self.omega()
        omega = self.stat["omega"] * particle_weight
        omega /= np.sum(omega)
        real_cmd = self.noise + self.origin_u  # (K,T,nu)
        weight_cmd = np.sum(omega.reshape(-1, 1, 1) * real_cmd, axis=0)  # (T,nu)
        return weight_cmd

    def state_aug(self, particle_weight):
        if "omega" not in self.stat:
            self.omega()
        omega = self.stat["omega"] * particle_weight
        omega /= np.sum(omega)
        mean = np.sum(omega.reshape(-1, 1, 1) * self.state, axis=0)  # (K,T,nx)->(T,nx)
        cov = np.einsum("kij,kit->kijt", (self.state - mean), (self.state - mean))
        cov = np.sum(omega.reshape(-1, 1, 1, 1) * cov, axis=0)
        self.stat["state_mean"] = mean
        self.stat["state_cov"] = cov
        return mean, cov

    def particle_update_weight(self):
        assert "first_step_cost" in self.stat
        loss = self.stat["first_step_cost"].copy()
        min_ = np.min(loss)
        ext_loss = min_ + np.log(np.mean(np.exp(-loss + min_)))

        loss -= min_
        weight = np.exp(-loss)
        eta = np.sum(weight)
        omega = 1.0 / eta * weight
        self.stat["omege_first"] = omega
        return omega

    def running_loss(self, R, dt, gamma=1.0):
        origin_u = np.stack(self._log["origin_u"], axis=-2)
        real_noise = np.stack(self._log["noise"], axis=-2)
        g_cost = np.stack(self._log["cost_g"]).T
        # action cost (K,T)
        action_cost_dt = np.einsum("kij,jt,kit->ki", origin_u, R, origin_u) * 0.5
        # noise cost (K,T)
        action_noise_cost_dt = np.einsum("kij,jt,kit->ki", origin_u, R, real_noise)
        g_cost_dt = g_cost * dt
        # cal noise cost
        noise_cost_dt = (
            np.einsum(
                "kij,jt,kit->ki",
                real_noise,
                R,
                real_noise,
            )
            * 0.5
            * (1 - 1.0 / gamma ** 2)
        ) * dt

        detail_costs = g_cost_dt + action_cost_dt + action_noise_cost_dt + noise_cost_dt
        total_cost = (
            np.sum(
                detail_costs,
                axis=1,
            )
            + self.terminal
        )
        first_step_cost = detail_costs[:, 0]

        self.stat.update(
            {
                "g_c": g_cost_dt,
                "a_c": action_cost_dt,
                "an_c": action_noise_cost_dt,
                "n_c": noise_cost_dt,
                "t_c": total_cost,
                "first_step_cost": first_step_cost,
            }
        )

        return (
            g_cost_dt,
            action_cost_dt,
            action_noise_cost_dt,
            noise_cost_dt,
            self.terminal,
            total_cost,
        )
