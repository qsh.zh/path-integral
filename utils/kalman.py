import numpy as np
import numpy.linalg as linalg
import attr

__all__ = ["KalmanFilter"]

@attr.s
class KalmanFilter:
    # notation follow wikipedia Fx + Bu + Q
    # y = Hx + R
    F = attr.ib()
    B = attr.ib()
    Q = attr.ib()
    H = attr.ib()
    R = attr.ib()

    mean = attr.ib()
    cov = attr.ib()

    def predict(self, x, cov, u):
        predicted_mean = self.F @ x + self.B @ u
        predicted_cov = self.F @ cov @ self.F.T + self.Q

        return predicted_mean, predicted_cov

    def update(self, p_mean, p_cov, y):
        residue = y - self.H @ p_mean
        inno = self.H @ p_cov @ self.H.T + self.R

        gain = p_cov @ self.H.T @ linalg.inv(inno)

        mean = p_mean + gain @ residue
        cov = (np.eye(mean.shape[0]) - gain @ self.H) @ p_cov

        # post_residue = y - self.H @ mean

        return mean, cov

    def filter(self, x, cov, u, y):
        p_mean, p_cov = self.predict(x, cov, u)
        # If there is no measurement
        if y is None:
            return p_mean, p_cov
        new_x, new_cov = self.update(p_mean, p_cov, y)

        return new_x, new_cov

    def estimate(self, u, y):
        # it assume a sinle along head
        self.mean, self.cov = self.filter(self.mean, self.cov, u, y)
        return self.mean, self.cov
