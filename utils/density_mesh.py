import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib import cm


def pmesh(y, T, weight):
    fig, axs = plt.subplots(1, 1)
    t = np.arange(0, T)
    y, x = np.meshgrid(y, t)
    z_min, z_max = -np.abs(weight).max(), np.abs(weight).max()
    c = axs.pcolormesh(
        x, y, weight, cmap="RdBu", vmin=z_min, vmax=z_max, shading="auto"
    )
    axs.axis = [x.min(), x.max(), y.min(), y.max()]
    fig.colorbar(c, ax=axs)
    return fig, axs


def viz_density(y, T, weight):
    fig = plt.figure()
    axs = fig.add_subplot(111, projection="3d")
    t = np.arange(0, T)
    origin_y = y
    y, x = np.meshgrid(y, t)
    z_min, z_max = -np.abs(weight).max(), np.abs(weight).max()
    c = axs.plot_surface(
        x,
        y,
        weight,
        #         cmap="RdBu",
        cmap=cm.coolwarm,
        vmin=z_min,
        vmax=z_max,
    )
    axs.axis = [x.min(), x.max(), y.min(), y.max()]
    axs.plot(
        np.arange(weight.shape[0]),
        -weight @ origin_y.T * (origin_y[0] - origin_y[1]),
        0,
    )
    fig.colorbar(c, ax=axs)
    return fig, axs
