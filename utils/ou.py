import numpy as np
import scipy.linalg as linalg
import attr
from jammy.utils.meta import run_once
from .batch_utils import handle_batch_input

__all__ = ["OU"]


@attr.s
class OU:
    # Follow notations in p.217
    lambda_ = attr.ib(default=0.5)
    dt = attr.ib(default=0.01)
    q = attr.ib(default=1.0)
    R = attr.ib(default=0.1)
    T = attr.ib(default=300)
    K = attr.ib(default=1)
    rng = attr.ib(default=None)
    m0 = attr.ib(default=0.0)
    p0 = attr.ib(default=1.0)

    @run_once
    def setup(self):
        self.m0 = np.array([self.m0]).flatten()
        self.p0 = np.array([self.p0]).reshape(self.m0.shape[0], -1)
        self.Q = np.array([self.transition_noise_sigma()]).reshape(
            len(self.m0), len(self.m0)
        ) # noise Sigma of transition
        self.R = np.array([self.R]).reshape(len(self.m0), len(self.m0))
        self.sqrt_QT = linalg.sqrtm(self.Q).T
        self.sqrt_RT = linalg.sqrtm(self.R).T
        self.A = np.array([self.transition()]).reshape(len(self.m0), len(self.m0)) # transition matrix

    def _process_init(self, x=None):
        self.setup()
        if x is None:
            x = self.m0 + linalg.sqrtm(self.p0) @ self.rng.normal(
                size=(self.K, len(self.m0))
            )
        assert x.shape[-1] == 1 and x.ndim == 2
        return x

    def sample_traj(self, x=None, T=None):
        """sample traj from OU process

        :param x: init state(K,nx), defaults to None
        :param T: Tdt, defaults to None
        :return: states (K,T+1,nx)
        """        
        # x.shape (K, nx)
        # return (K, T+1, nx)
        x = self._process_init(x)
        K = x.shape[0]
        rtn_state = [x]
        T = self.T if T is None else T
        for i in range(T):
            x = x @ self.A.T + self.rng.normal(size=(K, len(self.m0))) @ self.sqrt_QT
            rtn_state.append(x)

        return np.stack(rtn_state, axis=-2)

    @handle_batch_input
    def obser_traj(self, x):
        return self.rng.normal(size=x.shape) @ self.sqrt_RT + x

    @run_once
    def transition(self):
        return np.exp(-self.lambda_ * self.dt)

    @run_once
    def transition_noise_sigma(self):
        return self.q / (2 * self.lambda_) * (1 - np.exp(-2 * self.lambda_ * self.dt))

    @handle_batch_input
    def d_dyn_fn(self, x, u):
        return x @ self.A.T + u * self.dt @ self.sqrt_QT/np.sqrt(self.dt)

    @handle_batch_input
    def s_dyn_fn(self, x, u):
        u += self.rng.normal(x.shape) / np.sqrt(self.dt)
        return self.d_dyn_fn(x, u)

    @property
    def sde_sigma(self):
        # dx = sde_sigma du Using sigma due to the notation in Yongxin's Note
        # delta x = sqrt(Q/dt) u dt
        return self.sqrt_QT.T * np.sqrt(self.dt)

    @handle_batch_input
    def h(self, x, *args):
        return x
