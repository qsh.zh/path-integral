import numpy as np
import numpy.linalg as linalg
from einops import repeat
import attr
from jammy.utils.meta import run_once

# from .base_estimator import BaseEstimator

__all__ = ["BatchKalman"]


@attr.s
class BatchKalman:
    # notation follow wikipedia Fx + Bu + Q
    # y = Hx + R
    env = attr.ib()

    def __attrs_post_init__(self):
        self.F = self.env.A
        self.B = self.env.B
        self.Q = self.env.dyn_noise
        self.H = self.env.obs_gain
        self.R = self.env.obs_noise
        self.B_inv = np.linalg.inv(self.B)

        # cache for continuous estimation
        self.mean = None
        self.cov = None

    @property
    def nu(self):
        return self.B.shape[0]

    @property
    def nx(self):
        return self.B.shape[1]

    def predict(self, x, cov, u):
        """batch predict

        :param x: (K,nx)
        :param cov: (K,nx,nx)
        :param u: (K,nu)
        :return: p_x,p_cov
        """
        p_mean = np.einsum("ij,kj->ki", self.F, x) + np.einsum("ij,kj->ki", self.B, u)
        p_cov = np.einsum("ij,kjt,tu->kiu", self.F, cov, self.F.T) + self.Q[None, :]

        return p_mean, p_cov

    def update(self, p_mean, p_cov, y):
        """batch update

        :param p_mean: (K,nx)
        :param p_cov: (K,nx,nx)
        :param y: (K,ny)
        """
        residue = y - np.einsum("ij,kj->ki", self.H, p_mean)
        inno = (
            np.einsum("ij,kjt,tu->kiu", self.H, p_cov, self.H.T) + self.R[None, :]
        )  # kiu

        gain = np.einsum(
            "kij,jt,ktu->kiu", p_cov, self.H.T, linalg.inv(inno)
        )  # (K,nx,ny)
        mean = p_mean + np.einsum("kij,kj->ki", gain, residue)

        cov_coef = np.eye(mean.shape[1])[None, :] - np.einsum(
            "kij,ju->kiu", gain, self.H
        )  # (K,nx,nx)
        cov = np.einsum("kij,kju->kiu", cov_coef, p_cov)

        return mean, cov

    def oracle(self, x, cov, u, y):
        """batch filtering

        :param x: (K,nx)
        :param cov: (K,nx,nx)
        :param u: (K,nu)
        :param y: (K,ny)
        """
        p_mean, p_cov = self.predict(x, cov, u)
        if y is None:
            return p_mean, p_cov
        new_x, new_cov = self.update(p_mean, p_cov, y)

        return new_x, new_cov

    def filter(self, u, y, is_online=True):
        """batch estimate and NOT update cached mean cov

        :param u: (K,nu)
        :param y: (K,ny)
        """
        mean, cov = self.oracle(self.mean, self.cov, u, y)
        if is_online:
            self.mean, self.cov = mean, cov
        return mean, cov

    def step(self, u, y, is_online=True):
        mean, _ = self.filter(u, y, is_online)
        return mean

    def reset(self, mean, cov, K=1):
        if mean.ndim == 1:
            mean = mean[None, :]
            mean = repeat(mean, "i j->(c i) j", c=K)
        if cov.ndim == 2:
            cov = cov[None, :]
            cov = repeat(cov, "i j k->(c i) j k", c=K)
        assert mean.shape[1] == self.nx
        assert cov.shape[1] == self.nx
        self.mean = mean
        self.cov = cov

    @run_once
    def create_input(self, x):
        self.zero_input = np.zeros((x.shape[0], self.nu))

    def impl_ctrl(self, x, cov, y):
        """control command for estimation for one step

        :param x: last state mean (K,nx)
        :param cov: last state mean (K,nx,nx)
        :param y: current state observation (K,ny)
        :return: optimal control command from filtering(K,nu), new_x(K,nx),
            new_cov(K,nx,nx)
        """
        self.create_input(x)
        p_mean, p_cov = self.predict(x, cov, self.zero_input)
        new_x, new_cov = self.update(p_mean, p_cov, y)
        u = np.einsum("ij,kj->ki", self.B_inv, new_x - p_mean)
        # fmt: off
        # import ipdb; ipdb.set_trace()
        # fmt: on
        return u, new_x, new_cov

    def command(self, y, time_t, is_online=True):
        """interface for path integral

        :param y: (1,ny) observation
        :param is_online: whether or not save to cache, defaults to True
        :return: (K, nu) optimal control command
        """
        # FIXME:
        # y = repeat(y, 'i j->(c i) j', c=self.mean.shape[0])
        u, new_x, new_cov = self.impl_ctrl(self.mean, self.cov, y)
        if is_online:
            self.mean, self.cov = new_x, new_cov
        return u
