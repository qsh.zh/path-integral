import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams
import matplotlib._color_data as mcd

rcParams["backend"] = "Agg"
plt.style.use("paper")

__all__ = ["check_pi_cost", "debug_sampling"]


def check_pi_cost(traj_cost, action_cost, noise_cost, figname="check_pi_cost"):
    total_cost = traj_cost + action_cost + noise_cost
    sort_indx = np.argsort(total_cost)
    plt.plot(total_cost[sort_indx], label="Total")
    plt.plot(traj_cost[sort_indx], label="running")
    plt.plot(action_cost[sort_indx], label="action")
    plt.plot(noise_cost[sort_indx], label="noise")
    print(action_cost[sort_indx])
    plt.legend()

    plt.savefig(f"{figname}.png")


def debug_sampling(
    states, action, noise, origin_u, traj_cost, action_cost, noise_cost, figname
):
    # sampled_states (K,T,states)
    # traj_cost,xxx, (K,T)
    # fmf: off
    import ipdb

    ipdb.set_trace()
    # fmf: on
    traj_cost -= traj_cost[0]
    total_cost = np.sum(traj_cost + action_cost + noise_cost, axis=1)
    # total_cost -= np.min(total_cost)
    weight = np.exp(-total_cost)
    eta = np.sum(weight)
    omega = 1.0 / eta * weight

    cum_traj_cost = np.cumsum(traj_cost, axis=1)
    cum_action_cost = np.cumsum(action_cost, axis=1)
    cum_noise_cost = np.cumsum(noise_cost, axis=1)
    cum_total_cost = np.cumsum(traj_cost + action_cost + noise_cost, axis=1)

    fig, axes = plt.subplots(4, 2, figsize=(30, 20))
    # colors = list(mcd.CSS4_COLORS.values())
    colors = ["b", "g", "r", "c", "m", "y", "k", "darkorange"]
    num_traj = np.min([6, states.shape[0]])
    for i in range(num_traj):
        color = colors[i]
        axes[0, 0].plot(states[i, :, 0], color=color)
        axes[0, 1].plot(states[i, :, 1], color=color)

        axes[1, 0].plot(action[i], color=color)
        axes[1, 0].plot(origin_u[i], "-.", color=color, alpha=0.8)

        axes[1, 1].plot(noise[i], "-.", color=color, alpha=0.8)
        axes[1, 1].plot(noise[i] * omega[i], color=color)

        axes[2, 0].plot(cum_total_cost[i], "-.", color=color, alpha=0.8)
        axes[2, 0].plot(cum_traj_cost[i], color=color)

        axes[2, 1].plot(cum_total_cost[i], "-.", color=color, alpha=0.8)
        axes[2, 1].plot(cum_action_cost[i], color=color)

        axes[3, 0].plot(cum_total_cost[i], "-.", color=color, alpha=0.8)
        axes[3, 0].plot(cum_noise_cost[i], color=color)
    axes[3, 1].plot(np.sum(omega.reshape(-1, 1, 1) * noise, axis=0))

    axes[0, 0].set_title("x")
    axes[0, 1].set_title("v")
    axes[1, 0].set_title("u/origin_u")
    axes[1, 1].set_title("weight_n/n")
    axes[2, 0].set_title("traj/tc")
    axes[2, 1].set_title("u/tc")
    axes[3, 0].set_title("n/tc")
    plt.savefig(f"{figname}.png")
    plt.close(fig)
