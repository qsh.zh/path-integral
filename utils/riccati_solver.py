import numpy as np
import numpy.linalg as linalg

__all__ = ["fd_riccati_solver", "fd_riccati_solver_whole", "fd_riccati_solver_whole_F"]


def fd_riccati_solver(A, B, Q, R, T, dt):
    # https://en.wikipedia.org/wiki/Linear%E2%80%93quadratic_regulator
    # finite-horizon, discrete-time
    P = Q
    for _ in range(T - 1):
        P = (
            A.T @ P @ A
            - (A.T @ P @ B) @ linalg.inv(R + B.T @ P @ B) @ (B.T @ P @ A)
            + Q
        )
    F = linalg.inv((R + B.T @ P @ B)) @ (B.T @ P @ A)
    return F


def fd_riccati_solver_whole_F(A, B, Q, R, T, dt):
    list_P = fd_riccati_solver_whole(A, B, Q, R, T, dt)
    return list(map(lambda P: linalg.inv((R + B.T @ P @ B)) @ (B.T @ P @ A), list_P))


def fd_riccati_solver_whole(A, B, Q, R, T, dt):
    list_P = [Q]
    P = Q
    for _ in range(T - 1):
        P = (
            A.T @ P @ A
            - (A.T @ P @ B) @ linalg.inv(R + B.T @ P @ B) @ (B.T @ P @ A)
            + Q
        )
        list_P.append(P)
    list_P.reverse()
    return list_P


if __name__ == "__main__":
    dt = 0.05
    g_cov = 0.1

    g_A = np.array([[1, dt], [0, 1]])

    g_B = np.array([[0], [dt * np.sqrt(g_cov)]])

    g_Q = np.array([[10000, 0], [0, 0]])

    g_R = np.array([[1]])
    # print(fd_riccati_solver(g_A,g_B,g_Q,g_R,10))
    print(fd_riccati_solver_whole(g_A, g_B, g_Q, g_R, 10))
