import numpy as np
from jammy.random.rng import gen_rng

__all__ = ["NoiseDist"]


class NoiseDist:
    def __init__(self, dim, mean=None, cov=None):
        if mean is None:
            mean = np.zeros(dim)
        if cov is None:
            cov = np.eye(dim)

        assert mean.shape == (dim,)
        assert cov.shape == (dim, dim)

        self.rng = gen_rng(8)
        self.mean, self.cov = mean, cov

    def sample(self, size):
        return self.rng.multivariate_normal(self.mean, self.cov, size)
