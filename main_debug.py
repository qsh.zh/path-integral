import jammy.utils.hyd as hyd
import hydra
import numpy as np
from jamviz.plt import plotstd
import jammy.io as jio
from omegaconf import OmegaConf
from pathint.flt_pitgl import FltPItgl
from jammy.utils.debug import decorate_exception_hook
import matplotlib
import matplotlib.pyplot as plt
from utils.traj_book import DetailBook
from utils.density_mesh import pmesh
from tqdm.auto import tqdm


@decorate_exception_hook
def run(cfg):
    env = hyd.hyd_instantiate(cfg.env)
    controller = hyd.hyd_instantiate(cfg.option, env)
    hyd.hyd_instantiate(cfg.init, env, controller)()
    pi = FltPItgl(
        env, controller, cfg.K, cfg.T, samp_zero=cfg.samp_zero, log=DetailBook()
    )

    # TODO, adding env method support
    states, obser = env.sample(cfg.L)  # (1,L+1,nx/ny)
    jjf_data = jio.load(hyd.hydpath("data/jjf.pkl"))
    states[0, 1:] = jjf_data["ground_truth_track"].reshape(1, -1, 1)
    obser[0, 1:] = jjf_data["observed_track"].reshape(1, -1, 1)
    pi.prior_pts = env.prior_sample(cfg.K)
    # TODO: initialize data

    # Consider twice on how to implement the warmup
    # for-loop, remember to reset the controller states
    pi_mean, pi_cov = [], []
    for i in tqdm(range(cfg.L)):
        cur_obser = obser[:, i + 1]
        # for i, cur_obser in enumerate(obser[:, 1:]):
        mean, cov = pi.command(cur_obser, 0)
        pi_mean.append(mean)
        pi_cov.append(cov)

    # TODO: viz data
    fig, axs = plt.subplots(1, 1)
    # a, b, sigma_square, w = env.posterior(env.init_s[0], obser[0, :].flatten())
    d_mean, d_std = env.posterior(env.init_s[0], obser[0, :].flatten())
    # pos_range = np.linspace(-5, 5, 100)
    # density = env.plot(pos_range, a, b, sigma_square, w)
    # fig, axs = pmesh(pos_range, cfg.L, density)
    axs.plot(states[0, 1:], "r*-", label="gt", markersize=2)
    # axs.plot(obser[0, 1:] / 5, "k+", label="obs", markersize=6)
    # no need to plot the original estimation
    plotstd(
        pi_mean[:],
        2 * np.sqrt(pi_cov[:]).flatten(),
        color="gray",
        ax=axs,
        label="pi",
        markersize=2,
    )
    plotstd(d_mean, 2 * d_std, color="orange", label="post", ax=axs, markersize=2)
    # axs.plot(
    # density @ pos_range.T * (pos_range[1] - pos_range[0]), label="posterior_mean"
    # )
    # axs.plot(a - b, color="orange")
    # axs.plot(a + b, color="orange")
    # axs.plot(a, color="orange")
    axs.legend()
    # jio.dump(hyd.hydpath(f"data/density.pkl"), density)
    fig.savefig(hyd.hydpath(f"data/{cfg.figname}.png"))


@hydra.main(config_path="conf", config_name="filter.yaml")
def main(cfg):
    OmegaConf.set_struct(cfg, False)
    run(cfg)


if __name__ == "__main__":
    main()
